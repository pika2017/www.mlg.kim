/**
 * Created by Vnimy on 2016-11-17 .
 */


var CCGV = {
    MODULE: 'Colorcard',
    CONTROLLER: 'Admin'
};

var $fabric = $('#fabric');
var $tabs = $('#tabs');
var $newtabModal = $('#newtabModal');

$(document).ready(function(){
    initFabricDrag($fabric);
    initFabricDrop($fabric);
    initPaneDrop($tabs);
    $tabs.find('.tab-pane.removable').each(function(){
        pane = $(this);
        var $backbgbtn = '<button type="button" class="btn-u btn-u-lg"' +
            ' onclick="javascript:upload_bg(\'图片上传\',\'#'+pane.attr('id')+' .page-back:not(.front)\','+pane.data('id')+');"' +
            '>选择反面背景图片</button>';
        var $frontbgbtn = '<button type="button" class="btn-u btn-u-lg"' +
            ' onclick="javascript:upload_bg_front(\'图片上传\',\'#'+pane.attr('id')+' .page-back.front\','+pane.data('id')+');"' +
            '>选择正面背景图片</button>';
        pane.find('.page-back.front').append($frontbgbtn);
        pane.find('.page-back:not(.front)').append($backbgbtn);
    });

    $('.add-page',$newtabModal).on('click', function(){
        addPanel($(this).data('card_id'), $(this).data('pgtpl_id'));
    });

    $("#cardInfoForm").validate({
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                success:function(response){
                    if(response.status){
                        if(response.url){
                            location.href = response.url;
                        }else{
                            noty({text:response.info,type:'success'});
                        }
                    }else{
                        noty({text:response.info,type:'error'});
                    }
                }
            });
        }
    });
});

function setFrontCover(url){
    $('#frontCoverUrl').val(url);
    $('#front-cover').addClass('has-bg').css({'background-image':'url('+url+')'})
}

function setBackCover(url){
    $('#backCoverUrl').val(url);
    $('#back-cover').addClass('has-bg').css({'background-image':'url('+url+')'})
}

function initFabricDrag($fabric){
    $( ".fabric", $fabric ).draggable({
        revert: "invalid",
        containment: "document",
        helper: "clone",
        cursor: "move",
        zIndex: 100
    });
}

function initFabricDrop($fabric){
    $fabric.droppable({
        accept: "#fabric .fabric,#tabs .tab-pane .placeimg .fabric",
        classes: {
            "ui-droppable-active": "ui-state-highlight"
        },
        tolerance: 'touch',
        drop: function( event, ui ) {
            $item = ui.draggable;
            $target = $(event.target);
            dropToFabric($item,$target,true);
        }
    });
}

function initPaneDrop($pane){
    $('.placeimg',$pane).droppable({
        accept: "#fabric .fabric,#tabs .tab-pane .placeimg .fabric",
        classes: {
            "ui-droppable-active": "ui-state-highlight"
        },
        tolerance: 'pointer',
        drop: function( event, ui ) {
            $item = ui.draggable;
            $target = $(event.target);
            if($item.parent()[0] == $target[0]){
                return false;
            }
            $exist = $target.find('.fabric');
            console.log($exist.length);
            if($exist.length > 0){
                if($item.parent().attr('id') == 'fabric'){
                    dropToFabric($exist,$fabric,true);
                }else{
                    dropToPanel($exist,$item.parent(),true);
                }
                dropToPanel($item,$target,true);
            }else{
                dropToPanel($item,$target,true);
            }
        }
    });
}

function addPanel(card_id, pgtpl_id){
    var url = '/index.php?g='+CCGV.MODULE+'&m='+CCGV.CONTROLLER+'&a=add_page';
    var data = {id:card_id, tpl:pgtpl_id};
    $.post(url,data,function(response){
        if(response.status){
            $newtabModal.modal('hide');
            var index = $tabs.find('.nav-tabs li.removable').length+1;
            var nav = $('<li class="removable"><a href="#page_'+response.data.page_id+'" data-toggle="tab">'+index+'</a></li>');
            var pane = $(response.html);
            var $btnfront = '<button type="button" class="btn-u btn-u-lg" onclick="javascript:upload_bg_front(\'图片上传\',\'#'+pane.attr('id')+' .page-back.front\','+pane.data('id')+');">选择正面背景图片</button>';
            var $btn = '<button type="button" class="btn-u btn-u-lg" onclick="javascript:upload_bg(\'图片上传\',\'#'+pane.attr('id')+' .page-back:not(.front)\','+pane.data('id')+');">选择反面背景图片</button>';
            pane.find('.page-back.front').append($btnfront);
            pane.find('.page-back:not(.front)').append($btn);
            $tabs.find('.nav-tabs .newtab').before(nav);
            $tabs.find('.tab-content').append(pane);

            nav.find('a').tab('show');
            nav.parent().sortable({
                items: '>.removable',
                stop:function(){
                    refreshNums();
                }
            });
            tabsgo(1);
            initPaneDrop(pane);
        }else{
            noty({type:'error',text:response.info});
        }
    });
}

function removePanel(){
    var $nav = $('#tabs .nav-tabs li.removable.active');
    var $panel = $('#tabs .tab-content .removable.tab-pane.active');
    if($nav.length > 0 && $panel.length > 0){
        noty({
            text: '是否要删除该页？',
            layout: 'center',
            animation: {
                open: {height: 'toggle'},
                close: {height: 'toggle'},
                easing: 'swing',
                speed: 500
            },
            buttons: [
                {addClass: 'btn-u btn-u-primary',text: '确定',onClick: function($noty) {
                    $noty.close();
                    var url = '/index.php?g='+CCGV.MODULE+'&m='+CCGV.CONTROLLER+'&a=remove_page';
                    var data = {id:$panel.data('id')};
                    $.post(url,data,function(response){
                        if(response.status){
                            $panel.find('.fabric').each(function(){
                                dropToFabric($(this),$fabric,true);
                            });
                            $panel.prev().addClass('active');
                            $nav.prev().addClass('active');
                            $nav.remove();
                            $panel.remove();
                            refreshNums();
                        }else{
                            noty({type:'error',text:response.info});
                        }
                    });
                }},{addClass: 'btn-u btn-u-default',text: '取消',onClick: function($noty) {
                    $noty.close();
                }}
            ]
        });
    }
}

function refreshNums(){
    var $navs = $('#tabs .nav-tabs li.removable');
    var listorders = [];
    for(var i=0;i<$navs.length;i++){
        $navs.eq(i).find('a').text(i+1);
        listorders.push({id:$navs.eq(i).data('page_id'),value:i+1});
    }
    var url = '/index.php?g='+CCGV.MODULE+'&m='+CCGV.CONTROLLER+'&a=listorder';
    var data = {listorders:listorders};
    $.post(url,data,function(response){
        if(!response.status){
            noty({type:'error',text:response.info});
        }
    })
}

function dropToPanel($item,$target,sync,callback){
    var $target_block = $target.parents('.fabric-block');

    if (typeof callback !== "function") {
        callback = function(){};
    }

    if($item.parent().hasClass('placeimg')){
        cleanGrid($item.parents('.fabric-block'));
    }
    cleanGrid($target_block);
    $item.fadeOut(function() {
        $item.removeClass('col-xs-3').appendTo( $target ).fadeIn(callback);
        var qr_img = '/index.php?g=Tf&m=Tf&a=qr&code='+$item.data('code');
        $target_block.find('.qrcode').empty().append($('<img class="img-responsive" src="'+qr_img+'" />'));
        var data = $item.data();
        for(x in data){
            $target_block.find('.fabric-info .fabric-'+x).text(data[x])
        }
    });

    if (sync === true) {
        var url = '/index.php?g='+CCGV.MODULE+'&m='+CCGV.CONTROLLER+'&a=saveitem';
        var data = {
            tf_id: $item.data('tf_id'),
            item_id: $target_block.data('item_id')
        };
        $.post(url,data,function(response){
            if(!response.status){
                noty({type:'error',text:response.info});
                dropToFabric($item,$('#fabric'));
            }
        });
    }

    callback();
}

function dropToFabric($item,$target,sync,callback){
    if (typeof callback !== "function") {
        callback = function(){};
    }
    if($item.parent().hasClass('placeimg')){
        var $fabric_block = $item.parents('.fabric-block');
        cleanGrid($fabric_block);
        $item.fadeOut(function() {
            $item.addClass('col-xs-3').appendTo($target).fadeIn(callback);
            $( ".fabric", $fabric ).draggable({
                revert: "invalid",
                containment: "document",
                helper: "clone",
                cursor: "move",
                zIndex: 100
            });
        });

        if (sync === true) {
            var url = '/index.php?g='+CCGV.MODULE+'&m='+CCGV.CONTROLLER+'&a=saveitem';
            var data = {
                tf_id: 0,
                item_id: $fabric_block.data('item_id')
            };
            $.post(url,data,function(response){
                if(!response.status){
                    noty({type:'error',text:response.info});
                    dropToPanel($item,$fabric_block.find('.placeimg'),false);
                }
            });
        }
    }

    callback();
}

function cleanGrid($grid){
    $grid.find('.filltext,.qrcode').empty();
}

function tabsgo(lr) {
    if (lr){
        if($("#tabs .nav-tabs li:visible:last").offset().top>($('#tabs').offset().top+30))//防止全部被隐藏
            $("#tabs .nav-tabs li:visible:eq(0)").hide();//eq(0)为所有分类，永不隐藏
    }else
        $("#tabs .nav-tabs li:hidden:last").show();
}