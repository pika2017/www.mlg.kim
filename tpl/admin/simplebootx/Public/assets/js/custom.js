/**
 * Created by Vnimy on 2016-11-05 .
 */

$(function(){
    if(noty){
        $.noty.defaults = {
            layout: 'center',
            theme: 'relax', // or 'relax'
            type: 'alert',
            text: '', // can be html or string
            dismissQueue: true, // If you want to use queue feature set this true
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            animation: {
                open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
                close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            },
            timeout: 3000, // delay for closing event. Set false for sticky notifications
            force: false, // adds notification to the beginning of queue when set to true
            modal: false,
            maxVisible: 5, // you can set max visible notification for dismissQueue true option,
            killer: false, // for close all notifications before show
            closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
            callback: {
                onShow: function() {},
                afterShow: function() {},
                onClose: function() {},
                afterClose: function() {},
                onCloseClick: function() {},
            },
            buttons: false // an array of buttons
        };
    }

    if($.validator){
        $.extend($.validator.messages, {
            required: "这是必填字段",
            remote: "请修正此字段",
            email: "请输入有效的电子邮件地址",
            url: "请输入有效的网址",
            date: "请输入有效的日期",
            dateISO: "请输入有效的日期 (YYYY-MM-DD)",
            number: "请输入有效的数字",
            digits: "只能输入数字",
            creditcard: "请输入有效的信用卡号码",
            equalTo: "你的输入不相同",
            extension: "请输入有效的后缀",
            maxlength: $.validator.format("最多可以输入 {0} 个字符"),
            minlength: $.validator.format("最少要输入 {0} 个字符"),
            rangelength: $.validator.format("请输入长度在 {0} 到 {1} 之间的字符串"),
            range: $.validator.format("请输入范围在 {0} 到 {1} 之间的数值"),
            max: $.validator.format("请输入不大于 {0} 的数值"),
            min: $.validator.format("请输入不小于 {0} 的数值"),

            mobile: "请输入正确的手机号码"
        });

        $.validator.setDefaults({
            errorPlacement: function(error, element)
            {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    success:function(response){
                        if(response.status){
                            if(response.url){
                                location.href = response.url;
                            }else{
                                noty({text:response.info,type:'success'});
                            }
                        }else{
                            noty({text:response.info,type:'error'});
                        }
                    }
                });
            }
        });

        $.validator.addMethod(
            "mobile",
            function(value, element, required) {
                var re = new RegExp("(^(13\\d|15[^4\\D]|17[13678]|18\\d)\\d{8}|170[^346\\D]\\d{7})$");
                return this.optional(element) || required && re.test(value);
            },
            "请输入正确的手机号码！"
        );

        $.validator.addMethod(
            "inputVal",
            function(value, element, nodeName) {
                return $('[name="'+nodeName+'"]').val();
            },
            "请输入正确的值！"
        );
    }

    $('.js-ajax-btn').on('click', function (e) {
        e.preventDefault();
        var $_this = this,
            $this = $($_this),
            href = $this.data('href'),
            msg = $this.data('msg'),
            oktext = $this.data('oktext'),
            canceltext = $this.data('canceltext'),
            method = $this.data('method');
        data = {id:$this.data('id')};
        method = method ? method:'POST';
        href = href?href:$this.attr('href');
        if(!msg){
            msg="您确定要进行此操作吗？";
        }
        if(!oktext){
            oktext = '确定';
        }
        if(!canceltext){
            canceltext = '取消';
        }

        if($this.data('confirm') == true){
            noty({
                text: msg,
                layout: 'center',
                animation: {
                    open: {height: 'toggle'},
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 500
                },
                buttons: [
                    {addClass: 'btn-u btn-u-primary',text: oktext,onClick: function($noty) {
                        ajaxAction(href,data,method);
                        $noty.close();
                    }},{addClass: 'btn-u btn-u-default',text: canceltext,onClick: function($noty) {
                        $noty.close();
                    }}
                ]
            });
        }else{
            ajaxAction(href,data,method);
        }
    });


    $('body').on('click', '.el-selector .el-img', function(){
        elExplorer('selector',{eid:$(this).parent().attr('id')});
    });
});

function ajaxAction(url,data,method,success){
    if(typeof success !== "function"){
        success = function(response){
            if (response.state === 'success') {
                if (response.referer) {
                    location.href = response.referer;
                } else {
                    noty({type:'success',text:response.info});
                }
            } else if (response.state === 'fail') {
                noty({type:'error',text:response.info});
            }
        };
    }
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: method,
        success: success
    });
}

function cleanSubAreas(ele){
    var $ele = $('[name="'+ele+'"]');
    var subName = $ele.data('sub');
    if(subName != undefined){
        $('[name="'+subName+'"]').find('option:not(:disabled)').remove();
        cleanSubAreas(subName);
    }
}

function getAreas(id,target,callback){
    var $target = $('[name="'+target+'"]');
    $target.attr('disabled','disabled').parent().addClass('state-disabled');
    $.getJSON('/index.php?g=admin&m=public&a=get_areas',{id:id},function(response){
        var data = response.data;
        //$target.find('option:not(:disabled)').remove();
        for(x in data){
            $('<option value="'+data[x].id+'">'+data[x].name+'</option>').appendTo($target);
        }
        $target.removeAttr('disabled').parent().removeClass('state-disabled');

        if (typeof callback === "function"){
            callback();
        }
    });
}

function elExplorer(type,param){
    var url = '/index.php?g=admin&m=media&a=explorer&t='+type;
    switch(type) {
        case 'ckeditor':
            break;
        case 'selector':
            url += '&eid=' + param.eid;
            break;
        case 'call_func':
            url += '&func=' + param.func;
            break;
        default:
    }
    window.open(url,
        '_blank','toolbar=no, location=yes, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=yes, height=402');
}