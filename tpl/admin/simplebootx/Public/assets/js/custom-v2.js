/**
 * Created by Vnimy on 2016-11-05 .
 */

$(function () {

    $('.lazyload').lazyload();
    $.ajaxSetup({
        success: function (response) {
            if (response.url) {
                location.href = response.url;
            } else {
                if (response.status) {
                    if (typeof response.info !== "undefined" && response.info.length > 0) {
                        if (typeof noty == "function") {
                            noty({text: response.info, type: 'success'});
                        } else {
                            alert(response.info);
                        }
                    }
                } else {
                    if (typeof response.info !== "undefined" && response.info.length > 0) {
                        if (typeof noty == "function") {
                            noty({text: response.info, type: 'error'});
                        } else {
                            alert(response.info);
                        }
                    } else {
                        response.info = '操作出错！';
                    }
                }
            }
            if (typeof response.result == 'object') {
                result = response.result;
                for (x in result) {
                    if (typeof noty == "function") {
                        noty({text: result[x].msg, type: result[x].state});
                    } else {
                        alert(result[x].msg);
                    }
                }
            }
        }
    });


    if (typeof noty == "function") {
        $.noty.defaults = {
            layout: 'center',
            theme: 'relax', // or 'relax'
            type: 'alert',
            text: '', // can be html or string
            dismissQueue: true, // If you want to use queue feature set this true
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            animation: {
                open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
                close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            },
            timeout: 3000, // delay for closing event. Set false for sticky notifications
            force: false, // adds notification to the beginning of queue when set to true
            modal: false,
            maxVisible: 5, // you can set max visible notification for dismissQueue true option,
            killer: false, // for close all notifications before show
            closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
            callback: {
                onShow: function () {
                },
                afterShow: function () {
                },
                onClose: function () {
                },
                afterClose: function () {
                },
                onCloseClick: function () {
                },
            },
            buttons: false // an array of buttons
        };
        $.noty.themes.relax2 = $.extend({}, $.noty.themes.relax);
        $.noty.themes.relax2.name = 'relax2';
        $.noty.themes.relax2.style = function () {

            this.$bar.css({
                overflow: 'hidden',
                margin: '4px 0',
                borderRadius: '5px',
                padding: '30px 15px'
            });

            this.$message.css({
                fontSize: '24px',
                lineHeight: '28px',
                textAlign: 'center',
                padding: '10px',
                width: 'auto',
                position: 'relative',
                color: '#999',
                marginBottom: '40px'
            });

            this.$closeButton.css({
                position: 'absolute',
                top: 4, right: 4,
                width: 10, height: 10,
                background: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAAxUlEQVR4AR3MPUoDURSA0e++uSkkOxC3IAOWNtaCIDaChfgXBMEZbQRByxCwk+BasgQRZLSYoLgDQbARxry8nyumPcVRKDfd0Aa8AsgDv1zp6pYd5jWOwhvebRTbzNNEw5BSsIpsj/kurQBnmk7sIFcCF5yyZPDRG6trQhujXYosaFoc+2f1MJ89uc76IND6F9BvlXUdpb6xwD2+4q3me3bysiHvtLYrUJto7PD/ve7LNHxSg/woN2kSz4txasBdhyiz3ugPGetTjm3XRokAAAAASUVORK5CYII=)",
                display: 'none',
                cursor: 'pointer'
            });

            this.$buttons.css({
                padding: 5,
                textAlign: 'center',
                backgroundColor: '#fff'
            });

            this.$buttons.find('button').css({
                width: '130px',
                marginLeft: 8
            });

            this.$buttons.find('button:first').css({
                marginLeft: 0
            });

            this.$bar.on({
                mouseenter: function () {
                    $(this).find('.noty_close').stop().fadeTo('normal', 1);
                },
                mouseleave: function () {
                    $(this).find('.noty_close').stop().fadeTo('normal', 0);
                }
            });

            switch (this.options.layout.name) {
                case 'top':
                    this.$bar.css({
                        borderBottom: '2px solid #eee',
                        borderLeft: '2px solid #eee',
                        borderRight: '2px solid #eee',
                        borderTop: '2px solid #eee',
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    break;
                case 'topCenter':
                case 'center':
                case 'bottomCenter':
                case 'inline':
                    this.$bar.css({
                        border: '1px solid #eee',
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    //this.$message.css({fontSize: '24px', textAlign: 'center'});
                    break;
                case 'topLeft':
                case 'topRight':
                case 'bottomLeft':
                case 'bottomRight':
                case 'centerLeft':
                case 'centerRight':
                    this.$bar.css({
                        border: '1px solid #eee',
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    this.$message.css({fontSize: '13px', textAlign: 'left'});
                    break;
                case 'bottom':
                    this.$bar.css({
                        borderTop: '2px solid #eee',
                        borderLeft: '2px solid #eee',
                        borderRight: '2px solid #eee',
                        borderBottom: '2px solid #eee',
                        boxShadow: "0 -2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    break;
                default:
                    this.$bar.css({
                        border: '2px solid #eee',
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    break;
            }

            switch (this.options.type) {
                case 'alert':
                case 'notification':
                    this.$bar.css({backgroundColor: '#FFF', borderColor: '#dedede', color: '#444'});
                    break;
                case 'warning':
                    this.$bar.css({backgroundColor: '#FFEAA8', borderColor: '#FFC237', color: '#826200'});
                    this.$buttons.css({borderTop: '1px solid #FFC237'});
                    break;
                case 'error':
                    this.$bar.css({backgroundColor: '#FF8181', borderColor: '#e25353', color: '#FFF'});
                    this.$message.css({fontWeight: 'bold'});
                    this.$buttons.css({borderTop: '1px solid darkred'});
                    break;
                case 'information':
                    this.$bar.css({backgroundColor: '#78C5E7', borderColor: '#3badd6', color: '#FFF'});
                    this.$buttons.css({borderTop: '1px solid #0B90C4'});
                    break;
                case 'success':
                    this.$bar.css({backgroundColor: '#BCF5BC', borderColor: '#7cdd77', color: 'darkgreen'});
                    this.$buttons.css({borderTop: '1px solid #50C24E'});
                    break;
                default:
                    this.$bar.css({backgroundColor: '#FFF', borderColor: '#CCC', color: '#444'});
                    break;
            }
        };
    }

    if ($.validator) {
        $.extend($.validator.messages, {
            required: "这是必填字段",
            remote: "请修正此字段",
            email: "请输入有效的电子邮件地址",
            url: "请输入有效的网址",
            date: "请输入有效的日期",
            dateISO: "请输入有效的日期 (YYYY-MM-DD)",
            number: "请输入有效的数字",
            digits: "只能输入数字",
            creditcard: "请输入有效的信用卡号码",
            equalTo: "你的输入不相同",
            extension: "请输入有效的后缀",
            maxlength: $.validator.format("最多可以输入 {0} 个字符"),
            minlength: $.validator.format("最少要输入 {0} 个字符"),
            rangelength: $.validator.format("请输入长度在 {0} 到 {1} 之间的字符串"),
            range: $.validator.format("请输入范围在 {0} 到 {1} 之间的数值"),
            max: $.validator.format("请输入不大于 {0} 的数值"),
            min: $.validator.format("请输入不小于 {0} 的数值"),

            mobile: "请输入正确的手机号码"
        });

        $.validator.setDefaults({
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function (form) {
                if (typeof $(form).ajaxSubmit == "function") {
                    $(form).ajaxSubmit({
                        beforeSubmit: function (formData, $form, options) {
                            $form.find('[type="submit"]').attr('disabled', 'disabled').addClass('disabled');
                        },
                        success: function (response, statusText, xhr, $form) {
                            if (response.url) {
                                location.href = response.url;
                            } else {
                                if (response.status) {
                                    if (response.info.length > 0) {
                                        if (typeof noty == "function") {
                                            noty({text: response.info, type: 'success'});
                                        } else {
                                            alert(response.info);
                                        }
                                    }
                                } else {
                                    if (response.info.length > 0) {
                                        if (typeof noty == "function") {
                                            noty({text: response.info, type: 'error'});
                                        } else {
                                            alert(response.info);
                                        }
                                    } else {
                                        response.info = '操作出错！';
                                    }
                                }
                            }
                            $form.find('[type="submit"]').removeAttr('disabled', 'disabled').removeClass('disabled');
                        }
                    });
                }
            }
        });

        $.validator.addMethod(
            "mobile",
            function (value, element, required) {
                var re = new RegExp("(^(13\\d|15[^4\\D]|17[13678]|18\\d)\\d{8}|170[^346\\D]\\d{7})$");
                return this.optional(element) || required && re.test(value);
            },
            "请输入正确的手机号码！"
        );

        $.validator.addMethod(
            "inputVal",
            function (value, element, nodeName) {
                return $('[name="' + nodeName + '"]').val();
            },
            "请输入正确的值！"
        );
    }

    $('.js-ajax-btn').on('click', function (e) {
        e.preventDefault();
        var $_this = this,
            $this = $($_this),
            href = $this.data('href'),
            method = $this.data('method'),
            success = $this.data('success');
        data = {id: $this.data('id')};
        method = method ? method : 'POST';
        href = href ? href : $this.attr('href');

        if ($this.data('confirm') == true) {
            var msg = $this.data('msg'),
                oktext = $this.data('oktext'),
                canceltext = $this.data('canceltext'),
                confirm = $this.data('confirm');

            if (!msg) {
                msg = "您确定要进行此操作吗？";
            }
            if (!oktext) {
                oktext = '确定';
            }
            if (!canceltext) {
                canceltext = '取消';
            }

            if ($('body').has('.overlay').length > 0) {
                $('body>.overlay').show();
            } else {
                $('body').append('<div class="overlay"></div>');
            }

            noty({
                text: msg,
                layout: 'center',
                theme: 'relax2',
                animation: {
                    open: {height: 'toggle'},
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 500
                },
                buttons: [
                    {
                        addClass: 'btn-u btn-u-primary rounded', text: oktext, onClick: function ($noty) {
                        ajaxAction(href, data, method, success);
                        $noty.close();
                        if ($('body').has('.overlay').length > 0) {
                            $('body>.overlay').hide();
                        }
                    }
                    }, {
                        addClass: 'btn-u btn-u-default rounded',
                        text: canceltext, onClick: function ($noty) {
                            $noty.close();
                            if ($('body').has('.overlay').length > 0) {
                                $('body>.overlay').hide();
                            }
                        }
                    }
                ]
            });
        } else {
            ajaxAction(href, data, method, success);
        }
    });


    $('body').on('click', '.el-selector .el-img', function () {
        elExplorer('selector', {eid: $(this).parent().attr('id')});
    });
});
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        "h+": this.getHours(),                   //小时
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds()             //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
};

function ajaxAction(url, data, method, success) {
    if (success) {
        eval('try{success = ' + success + ';}catch(e){console.log(e.message)}');
    }
    if (typeof success !== "function") {
        success = function (response) {
            if (response.state === 'success') {
                if (response.referer) {
                    location.href = response.referer;
                } else {
                    noty({type: 'success', text: response.info});
                }
            } else if (response.state === 'fail') {
                noty({type: 'error', text: response.info});
            }
        };
    }

    if (!data) {
        data = {};
    }

    if (!method) {
        method = 'POST';
    }

    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: method,
        success: success
    });
}

function cleanSubAreas(ele) {
    var $ele = $('[name="' + ele + '"]');
    var subName = $ele.data('sub');
    if (subName != undefined) {
        $('[name="' + subName + '"]').find('option:not(:disabled)').remove();
        cleanSubAreas(subName);
    }
}

function getAreas(id, target, callback) {
    var $target = $('[name="' + target + '"]');
    $target.children().eq(0).attr('selected', 'selected');
    cleanSubAreas($('[data-sub="' + target + '"]').attr('name'));
    $target.attr('disabled', 'disabled').parent().addClass('state-disabled');
    $.getJSON('/index.php?g=portal&m=public&a=get_areas', {id: id}, function (response) {
        var data = response.data;
        //$target.find('option:not(:disabled)').remove();
        for (x in data) {
            $('<option value="' + data[x].id + '">' + data[x].name + '</option>').appendTo($target);
        }
        $target.removeAttr('disabled').parent().removeClass('state-disabled');

        if (typeof callback === "function") {
            callback();
        }
    });
}

function elExplorer(type, param) {
    var url = '/index.php?g=admin&m=media&a=explorer&t=' + type;
    switch (type) {
        case 'ckeditor':
            break;
        case 'selector':
            url += '&eid=' + param.eid;
            break;
        case 'call_func':
            url += '&func=' + param.func;
            break;
        default:
    }
    window.open(url,
        '_blank', 'toolbar=no, location=yes, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=yes, height=402');
}


/* -----------------------------
 * ThinkCMF 移植过来的函数
 ------------------------------*/


// 所有加了dialog类名的a链接，自动弹出它的href
if ($('a.js-dialog').length) {
    Wind.use('artDialog', 'iframeTools', function () {
        $('.js-dialog').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            art.dialog.open($(this).prop('href'), {
                close: function () {
                    $this.focus(); // 关闭时让触发弹窗的元素获取焦点
                    return true;
                },
                title: $this.prop('title')
            });
        }).attr('role', 'button');

    });
}

// 所有的ajax form提交,由于大多业务逻辑都是一样的，故统一处理
var ajaxForm_list = $('form.js-ajax-form');
if (ajaxForm_list.length) {
    Wind.use('ajaxForm', 'artDialog', 'validate', function () {

        var $btn;

        $('button.js-ajax-submit').on('click', function (e) {
            var btn = $(this), form = btn.parents('form.js-ajax-form');
            $btn = btn;

            if (btn.data("loading")) {
                return;
            }

            //批量操作 判断选项
            if (btn.data('subcheck')) {
                btn.parent().find('span').remove();
                if (form.find('input.js-check:checked').length) {
                    var msg = btn.data('msg');
                    if (msg) {
                        art.dialog({
                            id: 'warning',
                            icon: 'warning',
                            content: btn.data('msg'),
                            cancelVal: '关闭',
                            cancel: function () {
                                //btn.data('subcheck', false);
                                //btn.click();
                            },
                            ok: function () {
                                btn.data('subcheck', false);
                                btn.click();
                            }
                        });
                    } else {
                        btn.data('subcheck', false);
                        btn.click();
                    }

                } else {
                    $('<span class="tips_error">请至少选择一项</span>').appendTo(btn.parent()).fadeIn('fast');
                }
                return false;
            }

            //ie处理placeholder提交问题
            if ($.browser && $.browser.msie) {
                form.find('[placeholder]').each(function () {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });
            }
        });

        ajaxForm_list.each(function () {
            $(this).validate({
                //是否在获取焦点时验证
                //onfocusout : false,
                //是否在敲击键盘时验证
                onkeyup: function (element, event) {
                    return;

                    // Avoid revalidate the field when pressing one of the following keys
                    // Shift       => 16
                    // Ctrl        => 17
                    // Alt         => 18
                    // Caps lock   => 20
                    // End         => 35
                    // Home        => 36
                    // Left arrow  => 37
                    // Up arrow    => 38
                    // Right arrow => 39
                    // Down arrow  => 40
                    // Insert      => 45
                    // Num lock    => 144
                    // AltGr key   => 225
                    var excludedKeys = [
                        16, 17, 18, 20, 35, 36, 37,
                        38, 39, 40, 45, 144, 225
                    ];

                    if (event.which === 9 && this.elementValue(element) === "" || $.inArray(event.keyCode, excludedKeys) !== -1) {
                        return;
                    } else if (element.name in this.submitted || element.name in this.invalid) {
                        this.element(element);
                    }
                },
                //当鼠标掉级时验证
                onclick: false,
                //给未通过验证的元素加效果,闪烁等
                //highlight : false,
                showErrors: function (errorMap, errorArr) {
                    try {
                        $(errorArr[0].element).focus();
                        //alert(errorArr[0].message);
                    } catch (err) {
                    }
                },
                submitHandler: function (form) {
                    var $form = $(form);
                    $form.ajaxSubmit({
                        url: $btn.data('action') ? $btn.data('action') : $form.attr('action'), //按钮上是否自定义提交地址(多按钮情况)
                        dataType: 'json',
                        beforeSubmit: function (arr, $form, options) {

                            $btn.data("loading", true);
                            var text = $btn.text();

                            //按钮文案、状态修改
                            $btn.text(text + '中...').prop('disabled', true).addClass('disabled');
                        },
                        success: function (data, statusText, xhr, $form) {
                            var text = $btn.text();

                            //按钮文案、状态修改
                            $btn.removeClass('disabled').prop('disabled', false).text(text.replace('中...', '')).parent().find('span').remove();
                            if (data.state === 'success') {
                                $('<span class="tips_success">' + data.info + '</span>').appendTo($btn.parent()).fadeIn('slow').delay(1000).fadeOut(function () {
                                });
                            } else if (data.state === 'fail') {
                                var $verify_img = $form.find(".verify_img");
                                if ($verify_img.length) {
                                    $verify_img.attr("src", $verify_img.attr("src") + "&refresh=" + Math.random());
                                }

                                var $verify_input = $form.find("[name='verify']");
                                $verify_input.val("");

                                $('<span class="tips_error">' + data.info + '</span>').appendTo($btn.parent()).fadeIn('fast');
                                $btn.removeProp('disabled').removeClass('disabled');
                            }

                            if (data.referer) {
                                //返回带跳转地址
                                window.location.href = data.referer;
                            } else {
                                if (data.state === 'success') {
                                    //刷新当前页
                                    reloadPage(window);
                                }
                            }

                        },
                        error: function (xhr, e, statusText) {
                            alert(statusText);
                            //刷新当前页
                            reloadPage(window);
                        },
                        complete: function () {
                            $btn.data("loading", false);
                        }
                    });
                }
            });
        });

    });
}

//dialog弹窗内的关闭方法
$('#js-dialog-close').on('click', function (e) {
    e.preventDefault();
    try {
        art.dialog.close();
    } catch (err) {
        Wind.use('artDialog', 'iframeTools', function () {
            art.dialog.close();
        });
    }
    ;
});

/**
 * 读取cookie
 * @param name
 * @returns
 */
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }


    return null;
}

// 设置cookie
function setCookie(name, value, days) {
    var argc = setCookie.arguments.length;
    var argv = setCookie.arguments;
    var secure = (argc > 5) ? argv[5] : false;
    var expire = new Date();
    if (days == null || days == 0) days = 1;
    expire.setTime(expire.getTime() + 3600000 * 24 * days);
    document.cookie = name + "=" + escape(value) + ("; path=/") + ((secure == true) ? "; secure" : "") + ";expires=" + expire.toGMTString();
}

/**
 * 打开iframe式的窗口对话框
 * @param url
 * @param title
 * @param options
 */
function open_iframe_dialog(url, title, options) {
    var params = {
        title: title,
        lock: true,
        opacity: 0,
        width: "95%",
        height: '90%'
    };
    params = options ? $.extend(params, options) : params;
    Wind.use('artDialog', 'iframeTools', function () {
        art.dialog.open(url, params);
    });
}

/**
 * 打开地图对话框
 *
 * @param url
 * @param title
 * @param options
 * @param callback
 */
function open_map_dialog(url, title, options, callback) {

    var params = {
        title: title,
        lock: true,
        opacity: 0,
        width: "95%",
        height: 400,
        ok: function () {
            if (callback) {
                var d = this.iframe.contentWindow;
                var lng = $("#lng_input", d.document).val();
                var lat = $("#lat_input", d.document).val();
                var address = {};
                address.address = $("#address_input", d.document).val();
                address.province = $("#province_input", d.document).val();
                address.city = $("#city_input", d.document).val();
                address.district = $("#district_input", d.document).val();
                callback.apply(this, [lng, lat, address]);
            }
        }
    };
    params = options ? $.extend(params, options) : params;
    Wind.use('artDialog', 'iframeTools', function () {
        art.dialog.open(url, params);
    });
}

/**
 * 打开文件上传对话框
 * @param dialog_title 对话框标题
 * @param callback 回调方法，参数有（当前dialog对象，选择的文件数组，你设置的extra_params）
 * @param extra_params 额外参数，object
 * @param multi 是否可以多选
 * @param filetype 文件类型，image,video,audio,file
 * @param app  应用名，对于 CMF 的应用名
 */
function open_upload_dialog(dialog_title, callback, extra_params, multi, filetype, app) {
    multi = multi ? 1 : 0;
    filetype = filetype ? filetype : 'image';
    app = app ? app : GV.APP;
    var params = '&multi=' + multi + '&filetype=' + filetype + '&app=' + app;
    Wind.use("artDialog", "iframeTools", function () {
        art.dialog.open(GV.ROOT + 'index.php?g=asset&m=asset&a=plupload' + params, {
            title: dialog_title,
            id: new Date().getTime(),
            width: '650px',
            height: '420px',
            lock: true,
            fixed: true,
            background: "#CCCCCC",
            opacity: 0,
            ok: function () {
                if (typeof callback == 'function') {
                    var iframewindow = this.iframe.contentWindow;
                    var files = iframewindow.get_selected_files();
                    if (files) {
                        callback.apply(this, [this, files, extra_params]);
                    } else {
                        return false;
                    }

                }
            },
            cancel: true
        });
    });
}

function upload_one(dialog_title, input_selector, filetype, extra_params, app) {
    open_upload_dialog(dialog_title, function (dialog, files) {
        $(input_selector).val(files[0].filepath);
    }, extra_params, 0, filetype, app);
}

function upload_one_image(dialog_title, input_selector, extra_params, app) {
    open_upload_dialog(dialog_title, function (dialog, files) {
        $(input_selector).val(files[0].filepath);
        $(input_selector + '-preview').attr('src', files[0].preview_url);
        $(input_selector + '-name').val(files[0].name);
    }, extra_params, 0, 'image', app);
}

function up_img_for_search(dialog_title, input_selector, extra_params, app) {
    open_upload_dialog(dialog_title, function (dialog, files) {
        $(input_selector).val(files[0].filepath);

        $(input_selector + '_submit').click();

    }, extra_params, 0, 'image', app);
}
/**
 * 多图上传
 * @param dialog_title 上传对话框标题
 * @param container_selector 图片容器
 * @param item_tpl_wrapper_id 单个图片html模板容器id
 */
function upload_multi_image(dialog_title, container_selector, item_tpl_wrapper_id, extra_params, app) {
    open_upload_dialog(dialog_title, function (dialog, files) {
        var tpl = $('#' + item_tpl_wrapper_id).html();
        var html = '';
        $.each(files, function (i, item) {
            var itemtpl = tpl;
            itemtpl = itemtpl.replace(/\{id\}/g, item.id);
            itemtpl = itemtpl.replace(/\{url\}/g, item.url);
            itemtpl = itemtpl.replace(/\{preview_url\}/g, item.preview_url);
            itemtpl = itemtpl.replace(/\{filepath\}/g, item.filepath);
            itemtpl = itemtpl.replace(/\{name\}/g, item.name);
            html += itemtpl;
        });
        $(container_selector).append(html);

    }, extra_params, 1, 'image', app);
}

/**
 * 查看图片对话框
 * @param img 图片地址
 */
function image_preview_dialog(img) {
    Wind.use("artDialog", function () {
        art.dialog({
            title: '图片查看',
            fixed: true,
            width: "420px",
            height: '420px',
            id: "image_preview_" + img,
            lock: true,
            background: "#CCCCCC",
            opacity: 0,
            content: '<img src="' + img + '" />'
        });
    });
}

function artdialog_alert(msg) {
    Wind.use("artDialog", function () {
        art.dialog({
            id: new Date().getTime(),
            icon: "error",
            fixed: true,
            lock: true,
            background: "#CCCCCC",
            opacity: 0,
            content: msg,
            ok: function () {
                return true;
            }
        });
    });

}

function open_iframe_layer(url, title, options) {

    var params = {
        type: 2,
        title: title,
        shadeClose: true,
        skin: 'layui-layer-nobg',
        shade: [0.5, '#000000'],
        area: ['90%', '90%'],
        content: url
    };
    params = options ? $.extend(params, options) : params;

    Wind.css('layer');

    Wind.use("layer", function () {
        layer.open(params);
    });

}