$(function(){
    var $body = $('body');

    if(typeof $.fn.lazyload === "function"){
        $('.lazyload').lazyload();
    }

    if(noty){
        $.noty.defaults = {
            layout: 'center',
            theme: 'relax', // or 'relax'
            type: 'alert',
            text: '', // can be html or string
            dismissQueue: true, // If you want to use queue feature set this true
            template: '<div class="overlay"></div><div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            animation: {
                open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
                close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            },
            timeout: 3000, // delay for closing event. Set false for sticky notifications
            force: false, // adds notification to the beginning of queue when set to true
            modal: false,
            maxVisible: 5, // you can set max visible notification for dismissQueue true option,
            killer: false, // for close all notifications before show
            closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
            callback: {
                onShow: function() {
                    if(this.options.buttons.length > 0){
                        var $overlay = $('body > .overlay');
                        if($overlay.length > 0){
                            $overlay.show();
                        }else{
                            $('<div class="overlay">').appendTo($('body')).show();
                        }
                    }
                },
                afterShow: function() {},
                onClose: function() {},
                afterClose: function() {
                    if(this.options.buttons.length > 0){
                        var $overlay = $('body > .overlay');
                        if($overlay.length > 0){
                            $overlay.hide();
                        }
                    }
                },
                onCloseClick: function() {},
            },
            buttons: false // an array of buttons
        };
    }

    if($.validator){
        $.extend($.validator.messages, {
            required: "这是必填字段",
            remote: "请修正此字段",
            email: "请输入有效的电子邮件地址",
            url: "请输入有效的网址",
            date: "请输入有效的日期",
            dateISO: "请输入有效的日期 (YYYY-MM-DD)",
            number: "请输入有效的数字",
            digits: "只能输入数字",
            creditcard: "请输入有效的信用卡号码",
            equalTo: "你的输入不相同",
            extension: "请输入有效的后缀",
            maxlength: $.validator.format("最多可以输入 {0} 个字符"),
            minlength: $.validator.format("最少要输入 {0} 个字符"),
            rangelength: $.validator.format("请输入长度在 {0} 到 {1} 之间的字符串"),
            range: $.validator.format("请输入范围在 {0} 到 {1} 之间的数值"),
            max: $.validator.format("请输入不大于 {0} 的数值"),
            min: $.validator.format("请输入不小于 {0} 的数值"),

            mobile: "请输入正确的手机号码"
        });

        $.validator.setDefaults({
            errorPlacement: function(error, element)
            {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    success:function(response){
                        if(response.url){
                            location.href = response.url;
                        }else{
                            if(response.status){
                                noty({text:response.info,type:'success'});
                            }else{
                                noty({text:response.info,type:'error'});
                            }
                        }
                    }
                });
            }
        });

        $.validator.addMethod(
            "mobile",
            function(value, element, required) {
                var re = new RegExp("(^(13\\d|15[^4\\D]|17[13678]|18\\d)\\d{8}|170[^346\\D]\\d{7})$");
                return this.optional(element) || required && re.test(value);
            },
            "请输入正确的手机号码！"
        );

        $.validator.addMethod(
            "inputVal",
            function(value, element, nodeName) {
                return $('[name="'+nodeName+'"]').val();
            },
            "请输入正确的值！"
        );
    }

    $body.on('click', '.js-ajax-btn', function (e) {
        e.preventDefault();
        var $_this = this,
            $this = $($_this),
            href = $this.data('href'),
            method = $this.data('method'),
            success = $this.data('success');
        data = {id:$this.data('id')};
        method = method ? method:'POST';
        href = href?href:$this.attr('href');

        if($this.data('confirm') == true){
            var msg = $this.data('msg'),
                oktext = $this.data('oktext'),
                canceltext = $this.data('canceltext'),
                confirm = $this.data('confirm');

            if(!msg){
                msg="您确定要进行此操作吗？";
            }
            if(!oktext){
                oktext = '确定';
            }
            if(!canceltext){
                canceltext = '取消';
            }

            noty({
                text: msg,
                layout: 'center',
                animation: {
                    open: {height: 'toggle'},
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 500
                },
                buttons: [
                    {addClass: 'btn btn-red',text: oktext,onClick: function($noty) {
                        ajaxAction(href,data,method,success);
                        $noty.close();
                    }},{addClass: 'btn btn-bordered',text: canceltext,onClick: function($noty) {
                        $noty.close();
                    }}
                ]
            });
        }else{
            ajaxAction(href,data,method,success);
        }
    });


    $body.on('click', '.goods-number .btn-adjust', function () {
        var $this = $(this);
        var $block = $this.parent();
        var $number = $block.find('.inp-number');
        var num = parseInt($number.val());
        if($this.hasClass('add')){
            num++;
        }else{
            num--;
        }
        num = num > 0 ? num:1;
        if(num > 0){
            $number.val(num);
            $($block.data('response')).html(num);
        }

        if($this.parent().data('cb')){
            eval('var cb = '+$this.parent().data('cb'));
            if(typeof cb == "function"){
                cb($this.parent().data('id'),num);
            }
        }
    }).on('blur', '.goods-number .inp-number', function () {
        var $this = $(this);
        var $block = $this.parent();
        var num = parseInt($this.val());
        num = num > 0 ? num:1;
        $($block.data('response')).html(num);

        if($this.parent().data('cb')){
            eval('var cb = '+$this.parent().data('cb'));
            if(typeof cb == "function"){
                cb($this.parent().data('id'),num);
            }
        }
    });

    $body.on('click', '[data-toggle="slideFixedBox"]', function(e){
        e.preventDefault();
        var $box = $('.slidefixed-box'+$(this).attr('href'));
        if($box.data('overlay')){
            var $overlay = $('body > .overlay');
            if($overlay.length == 0){
                $overlay = $('<div class="overlay">').appendTo($('body'));
                $overlay.on('click',function(){
                    $box.find('.box-heading .close').click();
                });
            }
            $overlay.fadeIn(300);
        }

        $box.slideToggle(300);
    }).on('click', '.slidefixed-box .box-heading .close', function(){
        var $this = $(this);
        var $box = $this.parents('.slidefixed-box');
        $box.slideToggle(300);
        if($box.data('overlay')){
            var $overlay = $('body > .overlay');
            if($overlay.length > 0){
                $overlay.fadeOut(300);
            }
        }
    });

    $body.on('click', '[data-toggle="page"]', function(){
        $('.page').removeClass('active').filter($(this).attr('href')).addClass('active');
    });

    $body.on('click', '[data-toggle="select"]', function(){
        var $_this = $(this);
        var $target = $($_this.data('target'));
        $target.find('[name="'+$_this.data('name')+'"]').on('change', function(){
            $overlay.fadeOut(300);
            $target.fadeOut(300);
            $_this.find('span.text').text($(this).data('text'));
        });

        var $overlay = $('body > .overlay');
        if($overlay.length == 0){
            $overlay = $('<div class="overlay">').appendTo($('body'));
            $overlay.on('click',function(){
                $overlay.fadeOut(300);
                $target.fadeOut(300);
            });
        }
        $overlay.fadeIn(300);
        $target.fadeIn(300);
    });

    // Search Box (Header)
    $(".searchBox ul li").click(function () {
        $(this).addClass("cur").siblings().removeClass("cur")
    });

    $(".cameraBox").click(function () {
        $(".camera-slide").slideToggle();
    });
// End Search Box (Header)

});

function ajaxAction(url,data,method,success){
    if(success){
        eval('try{success = '+success+';}catch(e){console.log(e.message)}');
    }
    if(typeof success !== "function"){
        success = function(response){
            if (response.referer) {
                location.href = response.referer;
            } else {
                if (response.state === 'success') {
                    noty({type:'success',text:response.info});
                } else if (response.state === 'fail') {
                    noty({type:'error',text:response.info});
                }
            }
        };
    }

    if(!data){
        data = {};
    }

    if(!method){
        method = 'POST';
    }

    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: method,
        success: success
    });
}

function cleanSubAreas(ele){
    var $ele = $('[name="'+ele+'"]');
    var subName = $ele.data('sub');
    if(subName != undefined){
        $('[name="'+subName+'"]').find('option:not(:disabled)').remove();
        cleanSubAreas(subName);
    }
}

function getAreas(id,target,callback){
    var $target = $('[name="'+target+'"]');
    $target.children().eq(0).attr('selected','selected');
    cleanSubAreas($('[data-sub="'+target+'"]').attr('name'));
    $target.attr('disabled','disabled').parent().addClass('state-disabled');
    $.getJSON('/index.php?g=portal&m=public&a=get_areas',{id:id},function(response){
        var data = response.data;
        //$target.find('option:not(:disabled)').remove();
        for(x in data){
            $('<option value="'+data[x].id+'">'+data[x].name+'</option>').appendTo($target);
        }
        $target.removeAttr('disabled').parent().removeClass('state-disabled');

        if (typeof callback === "function"){
            callback();
        }
    });
}

/**
 * 打开文件上传对话框
 * @param dialog_title 对话框标题
 * @param callback 回调方法，参数有（当前dialog对象，选择的文件数组，你设置的extra_params）
 * @param extra_params 额外参数，object
 * @param multi 是否可以多选
 * @param filetype 文件类型，image,video,audio,file
 * @param app  应用名，对于 CMF 的应用名
 */
function open_upload_dialog(dialog_title,callback,extra_params,multi,filetype,app){
    multi = multi?1:0;
    filetype = filetype?filetype:'image';
    app = app?app:GV.APP;
    var params = '&multi='+multi+'&filetype='+filetype+'&app='+app ;
    art.dialog.open(GV.ROOT+'index.php?g=asset&m=UserAsset&a=plupload'  + params, {
        title: dialog_title,
        id: new Date().getTime(),
        width: '650px',
        height: '420px',
        lock: true,
        fixed: true,
        background:"#CCCCCC",
        opacity:0,
        ok: function() {
            if (typeof callback =='function') {
                var iframewindow = this.iframe.contentWindow;
                var files=iframewindow.get_selected_files();
                if(files){
                    callback.apply(this, [this, files,extra_params]);
                }else{
                    return false;
                }

            }
        },
        cancel: true
    });
}

function upload_one(dialog_title,input_selector,filetype,extra_params,app){
    open_upload_dialog(dialog_title,function(dialog,files){
        $(input_selector).val(files[0].filepath);
    },extra_params,0,filetype,app);
}

function upload_one_image(dialog_title,input_selector,extra_params,app){
    open_upload_dialog(dialog_title,function(dialog,files){
        $(input_selector).val(files[0].filepath);
        $(input_selector+'-preview').attr('src',files[0].preview_url);
        $(input_selector+'-name').val(files[0].name);
    },extra_params,0,'image',app);
}

function up_img_for_search(dialog_title,input_selector,extra_params,app){
    open_upload_dialog(dialog_title,function(dialog,files){
        $(input_selector).val(files[0].filepath);
        
        $(input_selector+'_submit').click();
        
    },extra_params,0,'image',app);
}




