/**
 * Created by Administrator on 2016/12/12.
 */
//rem单位兼容
/*(function(doc, win) {
 var docEl = doc.documentElement,
 resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
 recalc = function() {
 var clientWidth = docEl.clientWidth;
 if(!clientWidth) return;
 docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
 };
 if(!doc.addEventListener) return;
 win.addEventListener(resizeEvt, recalc, false);
 doc.addEventListener('DOMContentLoaded', recalc, false);
 })(document, window);*/


//头部tab切换
$(".order-tab ul li a").click(function () {
        $(".order-tab ul li a").removeClass("tab-active")
        $(this).addClass("tab-active");
    }
);
$(".comment-tab ul li a").click(function () {
        $(".comment-tab ul li a").removeClass("tab-active")
        $(this).addClass("tab-active");
    }
);
$(".recommend-tab ul li a").click(function () {
        $(".recommend-tab ul li a").removeClass("tab-active")
        $(this).addClass("tab-active");
    }
);


//收藏商品、供应商切换
$(".recommend-tab ul li a").click(function () {
    if($(".details-info-tab").hasClass("tab-active")){
        $(".parameter").hide();
        $(".details-info-con").show();
    }else {
        $(".details-info-con").hide();
        $(".parameter").show();
    }
});
//介绍与参数切换
$(".order-tab ul li a").click(function () {
    if($(".collection-shop").hasClass("tab-active")){
        $(".supplier-list").hide();
        $(".shop-list").show();
    }else {
        $(".shop-list").hide();
        $(".supplier-list").show();
    }
});
//记录、推荐切换
$(".recommend-tab ul li a").click(function () {
    if($(".recommend-tab-show").hasClass("tab-active")){
        $(".recommend-item2").hide();
        $(".recommend-item1").show();
    }else {
        $(".recommend-item1").hide();
        $(".recommend-item2").show();
    }
});


//删除弹提示框
$(".lj-delete,.delete-order,.shop-i-delete").click(function () {
    $(".delete-hint-b").show();
    $("body,html").css("overflow","hidden");
});
$(".delete-n").click(function () {
    $(".delete-hint-b").hide();
    $("body,html").css("overflow","");
});


//取消弹提示框
$(".cancel").click(function () {
    $(".delete-hint-b2").show();
    $("body,html").css("overflow","hidden");
});
$(".cancel-n").click(function () {
    $(".delete-hint-b2").hide();
    $("body,html").css("overflow","");
});


//轮播图

$(".order-owl").owlCarousel({
    items:5,
    margin:45
});

$("#commodity-owl").owlCarousel({
    items:1,
    stagePadding:0

});





//删除弹提示框
$(".delete-order").click(function () {
    $(".delete-hint-b").show();
    $("body,html").css("overflow","hidden");
});

//回到顶部
$(".to-top").click(function () {
    $(window).scrollTop(0);
});


//上下切换
$(".news .up").click(function () {
    if(!$(".news .up").hasClass("tab-active")){
        $(".news .up").css('transform','rotateX(0deg)');
        $(".news .up").addClass("tab-active");
        $(".choose-news").show();
    }else {
        $(".news .up").css('transform','rotateX(180deg)');
        $(".news .up").removeClass("tab-active");
        $(".choose-news").hide();
    }
});
$(".choose-item .up").click(function () {
    if(!$(".choose-item .up").hasClass("tab-active")){
        $(".choose-item .up").css('transform','rotateX(180deg)');
        $(".choose-item .up").addClass("tab-active");
        $(".promiseIco").addClass("show-prom");

    }else {
        $(".choose-item .up").css('transform','rotateX(0deg)');
        $(".choose-item .up").removeClass("tab-active");
        $(".promiseIco").removeClass("show-prom");
    }
});


//商品详情数量加减
$(".choose-item .reduce-num").click(function () {
    var num=parseInt($(".choose-item .c-news #number").val());
    if(num>1){
        $(".choose-item .c-news #number").val(num-1);
        $("#goods-num").html(num-1);
    }
    if(num==2){
        $(".choose-item .reduce-num").addClass("color-b");
    }
});
$(".choose-item .c-news #number").blur(function () {
    var num=parseInt($(".choose-item .c-news #number").val());
    if(num>1){
        $(".choose-item .reduce-num").removeClass("color-b");
    }else {
        $(".choose-item .reduce-num").addClass("color-b");
    }
    $("#goods-num").html(num);
});
$(".choose-item .add-num").click(function () {
    var num=parseInt($(".choose-item .c-news #number").val());
    $(".choose-item .c-news #number").val(num+1);
    $(".choose-item .reduce-num").removeClass("color-b");
    $("#goods-num").html(num+1);
});



//选择类型
$(".news-color a").click(function () {
        $(".news-color a").removeClass("my_choose")
        $(this).addClass("my_choose");
        $(".choose-color").html($(this).html());
    }
);
$(".news-version a").click(function () {
        $(".news-version a").removeClass("my_choose")
        $(this).addClass("my_choose");
        $(".choose-version").html($(this).html());
    }
);
$(".news-suit a").click(function () {
        $(".news-suit a").removeClass("my_choose")
        $(this).addClass("my_choose");
        $(".choose-suit").html($(this).html());
    }
);










//轮播图

$(".l-ing1").owlCarousel({
    items:5,
    margin:45
});
$("#l-img2").owlCarousel({
    items:3,
    margin:45
});

$("#commodity1-owl").owlCarousel({
    items:1,
    mouseDrag:false,
    touchDrag:false,
    URLhashListener: true,
    singleItem:true,
    rewindNav:true,
    startPosition:0,
    lazyLoad: true,
    autoHeight: true,
    startPosition: 'URLHash'
});

$("#commodity-owl,#recommend1-owl").owlCarousel({
    items:1,
    stagePadding:0

});

if(typeof Masterslider == "object"){
    var slider = new MasterSlider();
    slider.setup('masterslider', {
        width: 926,
        height:945,// slider standard width
        space: 5,
        autoplay:false,
        centerControls:true,
        start: 1,
        keyboard: false,
        wheel: false
    });
}