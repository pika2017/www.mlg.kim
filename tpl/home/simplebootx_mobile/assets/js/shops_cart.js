/**
 * Created by Administrator on 2016/12/20.
 */


/*购物车选择商品*/
//点击单个
$(".shops-choose1").click(function () {
    if($(this).hasClass("fa-square-o")){
        $(this).removeClass("fa-square-o").addClass("fa-check-square-o").addClass("color-c");
    }else {
        $(this).addClass("fa-square-o").removeClass("fa-check-square-o").removeClass("color-c");
    }

    //检查店铺内是否全选
    var a=0;
    var choose1=$(this).closest(".shop-i-list").find(".shops-choose1");
    if(choose1.length==1){
        $(this).closest(".shop-i-list").prev().find(".shops-choose").removeClass("fa-square-o").addClass("fa-check-square-o").addClass("color-c");
    }else {
        for(var i=0;i<choose1.length;i++){
            if($(".shops-choose1:eq("+i+")").hasClass("fa-check-square-o")){
                a++;
                if(a==choose1.length){
                    $(this).closest(".shop-i-list").prev().find(".shops-choose").removeClass("fa-square-o").addClass("fa-check-square-o").addClass("color-c");
                }else {
                    $(this).closest(".shop-i-list").prev().find(".shops-choose").addClass("fa-square-o").removeClass("fa-check-square-o").removeClass("color-c");
                }
            }
        }
    }
});
//点击全选
$(".all-check").click(function () {
    if($(this).hasClass("fa-square-o")){
        $(this).removeClass("fa-square-o").addClass("fa-check-square-o").addClass("color-c");
        $(".shops-choose,.shops-choose1").removeClass("fa-square-o").addClass("fa-check-square-o").addClass("color-c");
    }else {
        $(this).addClass("fa-square-o").removeClass("fa-check-square-o").removeClass("color-c");
        $(".shops-choose,.shops-choose1").addClass("fa-square-o").removeClass("fa-check-square-o").removeClass("color-c");
    }
});
//点击店铺
$(".shops-choose").click(function () {
    if($(this).hasClass("fa-square-o")){
        $(this).removeClass("fa-square-o").addClass("fa-check-square-o").addClass("color-c");
        $(this).parent().next().children().children().children(".shop-i-choose").children().removeClass("fa-square-o").addClass("fa-check-square-o").addClass("color-c");
    }else {
        $(this).addClass("fa-square-o").removeClass("fa-check-square-o").removeClass("color-c");
        $(this).parent().next().children().children().children(".shop-i-choose").children().addClass("fa-square-o").removeClass("fa-check-square-o").removeClass("color-c");
    }
});


//购物车合计金额
$(".shops-choose,.shops-choose1,.all-check").click(function () {
    count_rental();
    count_num();
    all_check();
});
//统计所有价格
function count_rental() {
    var shops_count=0;
    for(var i=0;i<$(".shops-choose1").length;i++){
        var check=$(".shops-choose1:eq("+i+")");
        if(check.hasClass("fa-check-square-o")){
            //alert(111);
            var count=parseFloat(check.parent().next().find(".add-up").html());
            shops_count=count+shops_count;
        }
    }
    //alert(shops_count.toFixed(2));
    $(".shops-rental").html(shops_count.toFixed(2));
}
//统计所有数量
function count_num() {
    var shops_num=0;
    for(var i=0;i<$(".shops-choose1").length;i++){
        var check=$(".shops-choose1:eq("+i+")");
        if(check.hasClass("fa-check-square-o")){
            //alert(111);
            var num=parseInt(check.parent().next().find(".p-buy-number").val());
            shops_num=num+shops_num;
        }
    }
    $("#count-num").html(shops_num);
}
//检查是否全选
function all_check() {
    var n=0;
    for(var i=0;i<$(".shops-choose1").length;i++){
        if($(".shops-choose1:eq("+i+")").hasClass("fa-check-square-o")){
            n++;
            if(n==$(".shops-choose1").length){
                $(".all-check").removeClass("fa-square-o").addClass("fa-check-square-o").addClass("color-c");
            }else {
                $(".all-check").addClass("fa-square-o").removeClass("fa-check-square-o").removeClass("color-c");
            }
        }
    }
}
