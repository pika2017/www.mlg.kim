/**
 * Created by Administrator on 2016/11/10.
 */
var validateForm = function () {
    return {
        //validataForm
        contactInformation: function () {
            //validate
            $('#validateForm1').validate({
                // Rules for form validation
                rules: {
                    contact_name: {
                        required: true
                    },
                    contact_mobile: {
                        required: true
                    },
                    contact_address: {
                        required: true
                    }
                },
                // Messages for form validation
                messages: {
                    contact_name: '请输入真实姓名',
                    contact_mobile: '请输入联系手机',
                    contact_address: '请输入详细地址'
                },
                // Do not change code below
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent());
                }
            });
        },

        shopInformation: function () {
            $('#validateForm2').validate({
                rules: {
                    long_name: 'required',
                    short_name: 'required',
                    company_address: 'required',
                    main_core: {required: !0, maxlength: 60},
                    logo: 'required',
                    shop_introduce: {required: !0, maxlength: 100}
                },
                messages: {
                    long_name: '请输入公司名称',
                    short_name: '请输入公司简称',
                    company_address: '请输入详细地址',
                    main_core: {
                        required: '请输入主营产品',
                        maxLength: '60字之内'
                    },
                    logo: '请输入企业标识',
                    shop_introduce: {
                        required: '请简单介绍您的企业',
                        maxLength: '100字之内'
                    }
                },
                // Do not change code below
                errorPlacement: function (error, element) {
                    if (element.is(":file")) {
                        error.appendTo(element.parent().parent().parent().parent().parent());
                    } else {
                        error.appendTo(element.is(":radio") || element.is(":checkbox") ? element.parent().parent().parent() : element.parent())
                    }
                }
            });
        },

        accountInformation: function () {
            $('#validateForm3').validate({
                rules: {
                    auth_fullname: 'required',
                    auth_bank_name: 'required',
                    auth_bank_branch: 'required',
                    auth_bank_card_no: 'required'
                },
                messages: {
                    auth_fullname: '请填写真实账户名',
                    auth_bank_name: '请选择银行',
                    auth_bank_branch: '请选择支行',
                    auth_bank_card_no: '请填写银行账号'
                },
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent());
                }
            });
        },

        releaseRequirements: function () {
            $('#validateForm4').validate({
                rules: {
                    "demand_img[]": 'required',
                    demand_end_date: 'required',
                    "demand_contact[name]": 'required',
                    "demand_contact[phone]":{ 
                        required: 1,
                        mobile: 1},
                },
                messages: {
                    "demand_img[]": '参考图片不能为空',
                    demand_end_date: '到期时间不能为空',
                    "demand_contact[name]": '联系人不能为空',
                    "demand_contact[phone]": '请输入正确的手机号码',
                },
                errorPlacement: function (error, element) {
                    if (element.is(":file")) {
                        error.appendTo(element.parent().parent().parent().parent().parent());
                    } else {
                        error.appendTo(element.is(":radio") || element.is(":checkbox") ? element.parent().parent().parent() : element.parent())
                    }
                }
            });
        },

        findPassword: function () {
            $("#findPassword").validate({
                rules: {
                    password: {
                        required: true,
                        maxlength: 20,
                        minlength: 6
                    },
                    surePassword: {
                        required: true,
                        equalTo: '#password'
                    }
                },
                messages: {
                    password: {
                        required:'请输入密码',
                        maxlength: '密码控制在20个字符内',
                        minlength: '密码不小于6个字符',
                    },
                    surePassword:{
                        required:'请再次输入密码',
                        equalTo:'两次输入的密码不一致'
                    }
                },
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent());
                }
            });
        },
        changePersonalDate:function () {
            $("#personalDate").validate({
                rules:{
                    short_name:'required',
                    long_name:'required',
                    mian_core:'required',
                    contact_name:'required',
                    contact_mobile:'required',
                    operationModel:'required',
                    mainBehavior:'required'
                },
                messages:{
                    short_name:'请输入商家简称',
                    long_name:'请输入全场称',
                    mian_core:'请输入主营',
                    contact_name:'请输入联系名称',
                    contact_mobile:'请输入联系手机',
                    operationModel:'请输入经营模式',
                    mainBehavior:'请输入主营行为'
                },
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent());
                }
            })
        },
    }
}();
