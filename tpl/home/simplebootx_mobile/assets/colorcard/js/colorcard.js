/**
 * Created by Vnimy on 2016-11-17 .
 */


var CCGV = {
    TPL_ROOT: "/tpl/home/simplebootx/Colorcard/pagetpl/"
};

var $fabric = $('#fabric');
var $tabs = $('#tabs');
var $newtabModal = $('#newtabModal');

$(document).ready(function(){
    initPaneDrop($tabs);

    $('.add-page',$newtabModal).on('click', function(){
        addPanel($(this).data('tpl'));
    });

    $( ".fabric", $fabric ).draggable({
        revert: "invalid",
        containment: "document",
        helper: "clone",
        cursor: "move",
        zIndex: 100
    });

    $fabric.droppable({
        accept: "#fabric .fabric,#tabs .tab-pane .thumbnail .fabric",
        classes: {
            "ui-droppable-active": "ui-state-highlight"
        },
        tolerance: 'touch',
        drop: function( event, ui ) {
            $item = ui.draggable;
            $target = $(event.target);
            dropToFabric($item,$target);
        }
    });

    $('.update-content').on('click',updateBook);

    $('#previewModal').on('show.bs.modal',function(){
        var data = getDataFromBook();
        if(data.pages.length > 0){
            for(x in data.pages){
                $page = data.pages[x];
                console.log($page);
            }
        }
    });


    $("#cardInfoForm").validate({
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                beforeSerialize:function(){
                    updateBook();
                    return true;
                },
                success:function(response){
                    if(response.status){
                        if(response.url){
                            location.href = response.url;
                        }else{
                            noty({text:response.info,type:'success'});
                        }
                    }else{
                        noty({text:response.info,type:'error'});
                    }
                }
            });
        }
    });
});

function setFrontCover(url){
    $('#frontCoverUrl').val(url);
    $('#front-cover').addClass('has-bg').css({'background-image':'url('+url+')'})
}

function setBackCover(url){
    $('#backCoverUrl').val(url);
    $('#back-cover').addClass('has-bg').css({'background-image':'url('+url+')'})
}

function initPaneDrop($pane){
    $('.thumbnail',$pane).droppable({
        accept: "#fabric .fabric,#tabs .tab-pane .thumbnail .fabric",
        classes: {
            "ui-droppable-active": "ui-state-highlight"
        },
        tolerance: 'pointer',
        drop: function( event, ui ) {
            $item = ui.draggable;
            $target = $(event.target);
            if($item.parent()[0] == $target[0]){
                return false;
            }
            $exist = $target.find('.fabric');
            console.log($exist.length);
            if($exist.length > 0){
                if($item.parent().attr('id') == 'fabric'){
                    dropToFabric($exist,$fabric);
                }else{
                    dropToPanel($exist,$item.parent());
                }
                dropToPanel($item,$target);
            }else{
                dropToPanel($item,$target);
            }
        }
    });
}

function addPanel(tpl){
    $.get('/index.php?g=Colorcard&m=Supplier&a=page_tpl&tpl='+tpl,function(response){
        $newtabModal.modal('hide');

        var timestamp = new Date().getTime();
        var id = 'page_'+timestamp;
        var index = $tabs.find('.nav-tabs li.removable').length+1;
        var nav = $('<li class="removable"><a href="#'+id+'" data-toggle="tab">'+index+'</a></li>');
        var pane = $(response).attr('id',id).addClass(tpl);
        $tabs.find('.nav-tabs .newtab').before(nav);
        $('#back-cover').before(pane);

        nav.find('a').tab('show');
        nav.parent().sortable({
            items: '>.removable',
            stop:function(){
                refreshNums();
            }
        });
        tabsgo(1);
        initPaneDrop(pane);
    });
}

function removePanel(){
    var $nav = $('#tabs .nav-tabs li.removable.active');
    var $panel = $('#tabs .tab-content .removable.tab-pane.active');
    if($nav.length > 0 && $panel.length > 0){
        noty({
            text: '是否要删除该页？',
            layout: 'center',
            animation: {
                open: {height: 'toggle'},
                close: {height: 'toggle'},
                easing: 'swing',
                speed: 500
            },
            buttons: [
                {addClass: 'btn-u btn-u-primary',text: '确定',onClick: function($noty) {
                    $panel.prev().addClass('active');
                    $nav.prev().addClass('active');
                    $nav.remove();
                    $panel.remove();
                    refreshNums();
                    $noty.close();
                }},{addClass: 'btn-u btn-u-default',text: '取消',onClick: function($noty) {
                    $noty.close();
                }}
            ]
        });
    }
}

function refreshNums(){
    var $navs = $('#tabs .nav-tabs li.removable');
    for(var i=0;i<$navs.length;i++){
        $navs.eq(i).find('a').text(i+1);
    }
}

function dropToPanel($item,$target,callback){
    var $target_block = $target.parents('.fabric-block');

    if (typeof callback !== "function") {
        callback = function(){};
    }

    if($item.parent().hasClass('placeimg')){
        cleanGrid($item.parents('.fabric-block'));
    }
    cleanGrid($target_block);
    $item.fadeOut(function() {
        $item.removeClass('col-xs-3').appendTo( $target ).fadeIn(callback);
        qr_img = '/index.php?g=Tf&m=Tf&a=qr&code='+$item.data('code');
        $target_block.find('.fabric-qr').empty().append($('<img class="img-responsive" src="'+qr_img+'" />'));
        data = $item.data();
        for(x in data){
            $target_block.find('.fabric-info .fabric-'+x+' span').text(data[x])
        }
    });
    callback();
}

function dropToFabric($item,$target,callback){
    if (typeof callback !== "function") {
        callback = function(){};
    }
    if($item.parent().hasClass('placeimg')){
        cleanGrid($item.parents('.fabric-block'));
        $item.fadeOut(function() {
            $item.addClass('col-xs-3').appendTo($target).fadeIn(callback);
        });
    }
}

function cleanGrid($grid){
    $grid.find('.fabric-qr').empty()
        .end().find('[class^="fabric-"] span').empty();
}

function getDataFromBook(){
    var pages = [];

    var $pageNavs = $('#tabs .nav .removable');
    $pageNavs.each(function(){
        $page = $($(this).find('a').attr('href'));
        pageData = {
            listorder: $(this).index(),
            page_tpl: $page.data('tpl'),
            items: []
        };
        if($page.data('page_id') > 0){
            //修改的分页
            pageData.page_id = $page.data('page_id');
        }
        $page.find('.fabric-block').each(function(){
            $item = $(this);
            itemData = {
                listorder: $item.index()
            };
            if($item.data('item_id') > 0){
                //修改的格子
                itemData.item_id = $item.data('item_id');
            }
            if($item.find('.placeimg').has('.fabric').length > 0){
                itemData.tf_id = parseInt($item.find('.placeimg .fabric').data('tf_id'));
                if(typeof itemData.tf_id != "number"){
                    itemData.tf_id = 0;
                }
            }else{
                itemData.tf_id = 0;
            }
            pageData.items.push(itemData);
        });
        pages.push(pageData);
    });
    return pages;
}

function updateBook(){
    data = getDataFromBook();
    var json = JSON.stringify(data);
    $('[name="pages"]').val(json);
    console.log(JSON.parse($('[name="pages"]').val()));
}

function tabsgo(lr) {
    if (lr){
        if($("#tabs .nav-tabs li:visible:last").offset().top>($('#tabs').offset().top+30))//防止全部被隐藏
            $("#tabs .nav-tabs li:visible:eq(0)").hide();//eq(0)为所有分类，永不隐藏
    }else
        $("#tabs .nav-tabs li:hidden:last").show();
}