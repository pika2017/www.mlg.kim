/**
 * Created by Vnimy on 2016-12-28 .
 */

var $login_popup = $('#login-popup');
if($login_popup.length){
    var check_login_status = false;

    $(".popup-wx-login").on("click",function () {
        $login_popup.show();
        if ($login_popup.is(':visible')) $('body').addClass('g-blur');
        refreshQrcode();
    });

    $login_popup.find('.btn-refresh').on('click', refreshQrcode);
    $login_popup.find('.g-popup__close').on('click', closeLoginPopup);
}

/*
 刷新登录二维码
 */
function refreshQrcode(){
    var d = new Date();
    $img = $login_popup.find('.img-responsive');
    $.get($img.data('src'),{t:d.getTime()},function(response){
        if(response.status){
            var data = response.data;
            $img
                .attr('data-sceneid',data.sceneid)
                .attr('data-ticket',data.ticket)
                .attr('src', data.src);
            $login_popup.find('.countdown').removeClass('hide');
            $login_popup.find('.expired').addClass('hide');
            $login_popup.find('.mask').addClass('hide');

            check_login_status = true;
            setTimeout(countdown, 1000, data.expire);
            handleLogin($img.data('login'),data.sceneid, data.ticket)
        }else{
            noty({text: response.info,type:'error'});
        }
    },'json');
}

function countdown(expire){
    var expire_date = new Date(expire);
    var now = new Date();
    var left = (Date.parse(expire_date) - Date.parse(now))/1000;
    $login_popup.find('.countdown .num').text(left);
    if(left <= 0){
        check_login_status = false;
        $login_popup.find('.countdown').addClass('hide');
        $login_popup.find('.expired').removeClass('hide');
        $login_popup.find('.mask').removeClass('hide');
    }
}

/*
 关闭登陆框
 */
function closeLoginPopup(){
    check_login_status = false;
    $login_popup.hide();
    $('body').removeClass('g-blur');
}

/*
 ajax登录操作
 */
function handleLogin(href,sceneid, ticket){
    if(check_login_status){
        data = {sceneid:sceneid,ticket:ticket};
        $.post(href,data,function(response){
            if(response.status){
                if(response.info[0] == 1){
                    check_login_status = false;
                    closeLoginPopup();
                    if(response.url){
                        location.href = response.url;
                    }else{
                        location.reload();
                    }
                }else if(response.info[0] == 0){
                    check_login_status = false;
                    closeLoginPopup();
                    noty({text: response.info[1],type:'error'});
                }else if(response.info[0] == 2){
                    check_login_status = false;
                    closeLoginPopup();
                    location.href = response.url;
                }
            }
        },'json');
        setTimeout(handleLogin,2000,href,sceneid, ticket)
    }
}