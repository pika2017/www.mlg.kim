$(function(){


    $('.lazyload').lazyload();

    $.ajaxSetup({
        success:function(response){
            if(response.url){
                location.href = response.url;
            }else{
                if(response.status){
                    if(typeof response.info !== "undefined" && response.info.length > 0){
                        if(typeof noty == "function"){
                            noty({text:response.info,type:'success'});
                        }else{
                            alert(response.info);
                        }
                    }
                }else{
                    if(typeof response.info !== "undefined" && response.info.length > 0){
                        if(typeof noty == "function"){
                            noty({text:response.info,type:'error'});
                        }else{
                            alert(response.info);
                        }
                    }else{
                        response.info = '操作出错！';
                    }
                }
            }
            if(typeof response.result == 'object'){
                result = response.result;
                for(x in result){
                    if(typeof noty == "function"){
                        noty({text:result[x].msg,type:result[x].state});
                    }else{
                        alert(result[x].msg);
                    }
                }
            }
        }
    });

    if(typeof noty == "function"){
        $.noty.defaults = {
            layout: 'center',
            theme: 'relax', // or 'relax'
            type: 'alert',
            text: '', // can be html or string
            dismissQueue: true, // If you want to use queue feature set this true
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            animation: {
                open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
                close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            },
            timeout: 3000, // delay for closing event. Set false for sticky notifications
            force: false, // adds notification to the beginning of queue when set to true
            modal: false,
            maxVisible: 5, // you can set max visible notification for dismissQueue true option,
            killer: false, // for close all notifications before show
            closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
            callback: {
                onShow: function() {},
                afterShow: function() {},
                onClose: function() {},
                afterClose: function() {},
                onCloseClick: function() {},
            },
            buttons: false // an array of buttons
        };
        $.noty.themes.relax2 = $.extend({}, $.noty.themes.relax);
        $.noty.themes.relax2.name = 'relax2';
        $.noty.themes.relax2.style = function() {

            this.$bar.css({
                overflow    : 'hidden',
                margin      : '4px 0',
                borderRadius: '5px',
                padding     : '30px 15px'
            });

            this.$message.css({
                fontSize  : '24px',
                lineHeight: '28px',
                textAlign : 'center',
                padding   : '10px',
                width     : 'auto',
                position  : 'relative',
                color     : '#999',
                marginBottom: '40px'
            });

            this.$closeButton.css({
                position  : 'absolute',
                top       : 4, right: 4,
                width     : 10, height: 10,
                background: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAAxUlEQVR4AR3MPUoDURSA0e++uSkkOxC3IAOWNtaCIDaChfgXBMEZbQRByxCwk+BasgQRZLSYoLgDQbARxry8nyumPcVRKDfd0Aa8AsgDv1zp6pYd5jWOwhvebRTbzNNEw5BSsIpsj/kurQBnmk7sIFcCF5yyZPDRG6trQhujXYosaFoc+2f1MJ89uc76IND6F9BvlXUdpb6xwD2+4q3me3bysiHvtLYrUJto7PD/ve7LNHxSg/woN2kSz4txasBdhyiz3ugPGetTjm3XRokAAAAASUVORK5CYII=)",
                display   : 'none',
                cursor    : 'pointer'
            });

            this.$buttons.css({
                padding        : 5,
                textAlign      : 'center',
                backgroundColor: '#fff'
            });

            this.$buttons.find('button').css({
                width: '130px',
                marginLeft: 8
            });

            this.$buttons.find('button:first').css({
                marginLeft: 0
            });

            this.$bar.on({
                mouseenter: function() {
                    $(this).find('.noty_close').stop().fadeTo('normal', 1);
                },
                mouseleave: function() {
                    $(this).find('.noty_close').stop().fadeTo('normal', 0);
                }
            });

            switch(this.options.layout.name) {
                case 'top':
                    this.$bar.css({
                        borderBottom: '2px solid #eee',
                        borderLeft  : '2px solid #eee',
                        borderRight : '2px solid #eee',
                        borderTop   : '2px solid #eee',
                        boxShadow   : "0 2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    break;
                case 'topCenter':
                case 'center':
                case 'bottomCenter':
                case 'inline':
                    this.$bar.css({
                        border   : '1px solid #eee',
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    //this.$message.css({fontSize: '24px', textAlign: 'center'});
                    break;
                case 'topLeft':
                case 'topRight':
                case 'bottomLeft':
                case 'bottomRight':
                case 'centerLeft':
                case 'centerRight':
                    this.$bar.css({
                        border   : '1px solid #eee',
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    this.$message.css({fontSize: '13px', textAlign: 'left'});
                    break;
                case 'bottom':
                    this.$bar.css({
                        borderTop   : '2px solid #eee',
                        borderLeft  : '2px solid #eee',
                        borderRight : '2px solid #eee',
                        borderBottom: '2px solid #eee',
                        boxShadow   : "0 -2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    break;
                default:
                    this.$bar.css({
                        border   : '2px solid #eee',
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)"
                    });
                    break;
            }

            switch(this.options.type) {
                case 'alert':
                case 'notification':
                    this.$bar.css({backgroundColor: '#FFF', borderColor: '#dedede', color: '#444'});
                    break;
                case 'warning':
                    this.$bar.css({backgroundColor: '#FFEAA8', borderColor: '#FFC237', color: '#826200'});
                    this.$buttons.css({borderTop: '1px solid #FFC237'});
                    break;
                case 'error':
                    this.$bar.css({backgroundColor: '#FF8181', borderColor: '#e25353', color: '#FFF'});
                    this.$message.css({fontWeight: 'bold'});
                    this.$buttons.css({borderTop: '1px solid darkred'});
                    break;
                case 'information':
                    this.$bar.css({backgroundColor: '#78C5E7', borderColor: '#3badd6', color: '#FFF'});
                    this.$buttons.css({borderTop: '1px solid #0B90C4'});
                    break;
                case 'success':
                    this.$bar.css({backgroundColor: '#BCF5BC', borderColor: '#7cdd77', color: 'darkgreen'});
                    this.$buttons.css({borderTop: '1px solid #50C24E'});
                    break;
                default:
                    this.$bar.css({backgroundColor: '#FFF', borderColor: '#CCC', color: '#444'});
                    break;
            }
        };
    }

    if($.validator){
        $.extend($.validator.messages, {
            required: "这是必填字段",
            remote: "请修正此字段",
            email: "请输入有效的电子邮件地址",
            url: "请输入有效的网址",
            date: "请输入有效的日期",
            dateISO: "请输入有效的日期 (YYYY-MM-DD)",
            number: "请输入有效的数字",
            digits: "只能输入数字",
            creditcard: "请输入有效的信用卡号码",
            equalTo: "你的输入不相同",
            extension: "请输入有效的后缀",
            maxlength: $.validator.format("最多可以输入 {0} 个字符"),
            minlength: $.validator.format("最少要输入 {0} 个字符"),
            rangelength: $.validator.format("请输入长度在 {0} 到 {1} 之间的字符串"),
            range: $.validator.format("请输入范围在 {0} 到 {1} 之间的数值"),
            max: $.validator.format("请输入不大于 {0} 的数值"),
            min: $.validator.format("请输入不小于 {0} 的数值"),

            mobile: "请输入正确的手机号码"
        });

        $.validator.setDefaults({
            errorPlacement: function(error, element)
            {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                if(typeof $(form).ajaxSubmit == "function"){
                    $(form).ajaxSubmit({
                        beforeSubmit: function(formData, $form, options) {
                            $form.find('[type="submit"]').attr('disabled', 'disabled').addClass('disabled');
                        },
                        success:function(response, statusText, xhr, $form){
                            if(response.url){
                                location.href = response.url;
                            }else{
                                if(response.status){
                                    if(response.info.length > 0){
                                        if(typeof noty == "function"){
                                            noty({text:response.info,type:'success'});
                                        }else{
                                            alert(response.info);
                                        }
                                    }
                                }else{
                                    if(response.info.length > 0){
                                        if(typeof noty == "function"){
                                            noty({text:response.info,type:'error'});
                                        }else{
                                            alert(response.info);
                                        }
                                    }else{
                                        response.info = '操作出错！';
                                    }
                                }
                            }
                            $form.find('[type="submit"]').removeAttr('disabled', 'disabled').removeClass('disabled');
                        }
                    });
                }
            }
        });

        $.validator.addMethod(
            "mobile",
            function(value, element, required) {
                var re = new RegExp("(^(13\\d|15[^4\\D]|17[13678]|18\\d)\\d{8}|170[^346\\D]\\d{7})$");
                return this.optional(element) || required && re.test(value);
            },
            "请输入正确的手机号码！"
        );

        $.validator.addMethod(
            "inputVal",
            function(value, element, nodeName) {
                return $('[name="'+nodeName+'"]').val();
            },
            "请输入正确的值！"
        );
    }

    $('body').on('click', '.js-ajax-btn', function (e) {
        e.preventDefault();
        var $_this = this,
            $this = $($_this),
            href = $this.data('href'),
            method = $this.data('method'),
            success = $this.data('success');
        data = {id:$this.data('id')};
        method = method ? method:'POST';
        href = href?href:$this.attr('href');

        if($this.data('confirm') == true){
            var msg = $this.data('msg'),
                oktext = $this.data('oktext'),
                canceltext = $this.data('canceltext'),
                confirm = $this.data('confirm');

            if(!msg){
                msg="您确定要进行此操作吗？";
            }
            if(!oktext){
                oktext = '确定';
            }
            if(!canceltext){
                canceltext = '取消';
            }

            if($('body').has('.overlay').length > 0){
                $('body>.overlay').show();
            }else{
                $('body').append('<div class="overlay"></div>');
            }

            noty({
                text: msg,
                layout: 'center',
                theme: 'relax2',
                animation: {
                    open: {height: 'toggle'},
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 500
                },
                buttons: [
                    {addClass: 'btn-u btn-u-primary rounded',text: oktext,onClick: function($noty) {
                        ajaxAction(href,data,method,success);
                        $noty.close();
                        if($('body').has('.overlay').length > 0){
                            $('body>.overlay').hide();
                        }
                    }},{addClass: 'btn-u btn-u-default rounded',
                        text: canceltext,onClick: function($noty) {
                        $noty.close();
                        if($('body').has('.overlay').length > 0){
                            $('body>.overlay').hide();
                        }
                    }}
                ]
            });
        }else{
            ajaxAction(href,data,method,success);
        }
    });

    var $qrcode_popup = $('#qrcode-popup');
    $('#qrcodecontrol').on('click', function(){
        $qrcode_popup.show();
        //if ($qrcode_popup.is(':visible')) $('body').addClass('g-blur');
    });
    $qrcode_popup.find('.g-popup__close').on('click', function(){
        $qrcode_popup.hide();
        //$('body').removeClass('g-blur');
    });


    /* popup */
    $('[data-toggle="g-popup"]').on('click',function(){
        showGPopup( $($(this).data('target')))
    });

    /* 登陆 */
    var $login_popup = $('#login-popup');
    if($login_popup.length){
        var check_login_status = false;

        $(".popup-wx-login").on("click",function () {
            $login_popup.show();
            if ($login_popup.is(':visible')) $('body').addClass('g-blur');
            refreshQrcode();
        });

        $login_popup.find('.btn-refresh').on('click', refreshQrcode);
        $login_popup.find('.g-popup__close').on('click', closeLoginPopup);

        if(typeof popupLoginNow === "boolean"){
            if(popupLoginNow === true){
                popupLoginNow = false;
                $(".popup-wx-login").click();
            }
        }

        /*
         刷新登录二维码
         */
        function refreshQrcode(){
            var d = new Date();
            $img = $login_popup.find('.img-responsive');
            $.get($img.data('src'),{t:d.getTime()},function(response){
                if(response.status){
                    var data = response.data;
                    $img
                        .attr('data-sceneid',data.sceneid)
                        .attr('data-ticket',data.ticket)
                        .attr('src', data.src);
                    $login_popup.find('.countdown').removeClass('hide');
                    $login_popup.find('.expired').addClass('hide');
                    $login_popup.find('.mask').addClass('hide');

                    check_login_status = true;
                    countdown(data.expire);
                    handleLogin($img.data('login'), data.id)
                }else{
                    noty({text: response.info,type:'error'});
                }
            },'json');
        }

        function countdown(expire){
            // var expire_date = new Date(expire);
            var str = expire.replace(/-/g,'/');
            var expire_date = new Date(str);
            
            var now = new Date();
            var left = (Date.parse(expire_date) - Date.parse(now))/1000;
            $login_popup.find('.countdown .num').text(left);
            if(left <= 0){
                check_login_status = false;
                $login_popup.find('.countdown').addClass('hide');
                $login_popup.find('.expired').removeClass('hide');
                $login_popup.find('.mask').removeClass('hide');
            }else{
                setTimeout(countdown, 1000, expire);
            }
        }

        /*
         关闭登陆框
         */
        function closeLoginPopup(){
            check_login_status = false;
            $login_popup.hide();
            $('body').removeClass('g-blur');
        }

        /*
         ajax登录操作
         */
        function handleLogin(href, id){
            if(check_login_status){
                data = {id:id};
                $.getJSON(href, data, function(response){
                    if(response.status){
                        if(response.data == 1){
                            check_login_status = false;
                            closeLoginPopup();
                            if(response.url){
                                location.href = response.url;
                            }else{
                                location.reload();
                            }
                        }else if(response.data == 0){
                            check_login_status = false;
                            closeLoginPopup();
                            noty({text: response.info,type:'error'});
                        }else if(response.data == 2){
                            check_login_status = false;
                            closeLoginPopup();
                            location.href = response.url;
                        }
                    }
                },'json');
                setTimeout(handleLogin,2000,href,id)
            }
        }
    }

    /* 绑定手机popup */
    $popupMobile = $('#fill-mobile');
    if($popupMobile.length > 0){
        if(typeof popupMobileNow !== "undefined"){
            if(popupMobileNow === true){
                popupMobileNow = false;
                showGPopup($popupMobile);
            }
        }
        $popupMobile.find('form').validate({
            rules: {
                mobile: {
                    required:1,
                    mobile:1
                },
                verify: {
                    required: 1
                }
            },
            messages: {
                mobile: {
                    required: "请输入手机号码",
                    mobile: "请输入正确的手机号码"
                },
                verify: {
                    required: "请输入验证码"
                }
            },
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            },
            submitHandler: function(form) {
                if(typeof $(form).ajaxSubmit == "function"){
                    $(form).ajaxSubmit({
                        success:function(response){
                            if(response.url){
                                location.href = response.url;
                            }else{
                                if(response.status){
                                    noty({text:response.info,type:'success'});
                                    $popupMobile.find('.g-popup__close').click();
                                }else{
                                    noty({text:response.info,type:'error'});
                                }
                            }
                        }
                    });
                }
            }
        });
        $popupMobile.find('.btn-verify:not(:disabled)').on('click',function(){
            var $_this = $(this);
            var $mobile = $popupMobile.find('[name="mobile"]');
            if($mobile.valid()){
                ajaxGetVerifyCode($mobile.val(), $_this);
            }
        });
    }

    $('body').on('click', '[data-toggle="details"]', function(){
        var $this = $(this);
        var $target = $($this.data('target'));
        $target.toggleClass('open');
        if($target.hasClass('open')){
            $this.find('i.fa')
                .removeClass('fa-angle-double-down')
                .addClass('fa-angle-double-up');
        }else{
            $this.find('i.fa')
                .removeClass('fa-angle-double-up')
                .addClass('fa-angle-double-down');
        }
    });

});

/* 显示 g-popup */
function showGPopup($target){
    $target.show();
    if ($target.is(':visible')) $('body').addClass('g-blur');
    $target.find('.g-popup__close').on('click', function(e) {
        $target.hide();
        $('body').removeClass('g-blur');
    });
}

/* 手机验证码倒计时 */
function verifyCodeCountdown(dt,btn){
    dt--;
    if(!btn.prop('disabled')){
        btn.prop('disabled',true);
    }
    btn.text(dt+"秒后可重发");
    if(dt > 0){
        setTimeout(verifyCodeCountdown,1000,dt,btn);
    }else{
        btn.prop('disabled',false);
        btn.text('获取验证码')
    }
}

function ajaxGetVerifyCode(to, btn){
    btn.prop('disabled',true);
    var url = '/index.php?g=Sms&m=Send&a=send_code';
    var data = {to:to};
    $.post(url,data,function(response){
        if(response.status){
            verifyCodeCountdown(60,btn);
        }else{
            btn.prop('disabled',false);
            noty({type:'error',text:response.info});
        }
    });
}

function ajaxAction(url,data,method,success){
    if(success){
        eval('try{success = '+success+';}catch(e){console.log(e.message)}');
    }
    if(typeof success !== "function"){
        success = function(response){
            if (response.referer) {
                location.href = response.referer;
            } else {
                if (response.state === 'success') {
                    noty({type:'success',text:response.info});
                } else if (response.state === 'fail') {
                    noty({type:'error',text:response.info});
                }
            }
        };
    }

    if(!data){
        data = {};
    }

    if(!method){
        method = 'POST';
    }

    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: method,
        success: success
    });
}

function cleanSubAreas(ele){
    var $ele = $('[name="'+ele+'"]');
    var subName = $ele.data('sub');
    if(subName != undefined){
        $('[name="'+subName+'"]').find('option:not(:disabled)').remove();
        cleanSubAreas(subName);
    }
}

function getAreas(id,target,callback){
    var $target = $('[name="'+target+'"]');
    $target.children().eq(0).attr('selected','selected');
    cleanSubAreas($('[data-sub="'+target+'"]').attr('name'));
    $target.attr('disabled','disabled').parent().addClass('state-disabled');
    $.getJSON('/index.php?g=portal&m=public&a=get_areas',{id:id},function(response){
        var data = response.data;
        //$target.find('option:not(:disabled)').remove();
        for(x in data){
            $('<option value="'+data[x].id+'">'+data[x].name+'</option>').appendTo($target);
        }
        $target.removeAttr('disabled').parent().removeClass('state-disabled');

        if (typeof callback === "function"){
            callback();
        }
    });
}

/**
 * 打开文件上传对话框
 * @param dialog_title 对话框标题
 * @param callback 回调方法，参数有（当前dialog对象，选择的文件数组，你设置的extra_params）
 * @param extra_params 额外参数，object
 * @param multi 是否可以多选
 * @param filetype 文件类型，image,video,audio,file
 * @param app  应用名，对于 CMF 的应用名
 */
function open_upload_dialog(dialog_title,callback,extra_params,multi,filetype,app){
    multi = multi?1:0;
    filetype = filetype?filetype:'image';
    app = app?app:GV.APP;
    var params = '&multi='+multi+'&filetype='+filetype+'&app='+app ;
    art.dialog.open(GV.ROOT+'index.php?g=asset&m=index&a=index'  + params, {
        title: dialog_title,
        id: new Date().getTime(),
        width: '650px',
        height: '420px',
        lock: true,
        fixed: true,
        background:"#CCCCCC",
        opacity:0,
        ok: function() {
            if (typeof callback =='function') {
                var iframewindow = this.iframe.contentWindow;
                var files=iframewindow.get_selected_files();
                if(files){
                    callback.apply(this, [this, files,extra_params]);
                }else{
                    return false;
                }

            }
        },
        cancel: true
    });
}

/**
 * 多图上传
 * @param dialog_title 上传对话框标题
 * @param container_selector 图片容器
 * @param item_tpl_wrapper_id 单个图片html模板容器id
 */
function upload_multi_image(dialog_title,container_selector,item_tpl_wrapper_id,extra_params,app){
    open_upload_dialog(dialog_title,function(dialog,files){
        var tpl=$('#'+item_tpl_wrapper_id).html();
        var html='';
        $.each(files,function(i,item){
            var itemtpl= tpl;
            itemtpl=itemtpl.replace(/\{id\}/g,item.id);
            itemtpl=itemtpl.replace(/\{url\}/g,item.url);
            itemtpl=itemtpl.replace(/\{preview_url\}/g,item.preview_url);
            itemtpl=itemtpl.replace(/\{filepath\}/g,item.filepath);
            itemtpl=itemtpl.replace(/\{name\}/g,item.name);
            html+=itemtpl;
        });
        $(container_selector).append(html);
        
    },extra_params,1,'image',app);
}

function upload_one(dialog_title,input_selector,filetype,extra_params,app){
    open_upload_dialog(dialog_title,function(dialog,files){
        $(input_selector).val(files[0].filepath);
    },extra_params,0,filetype,app);
}

function upload_one_image(dialog_title,input_selector,extra_params,app){
    open_upload_dialog(dialog_title,function(dialog,files){
        $(input_selector).val(files[0].filepath);
        $(input_selector+'-preview').attr('src',files[0].preview_url);
        $(input_selector+'-name').val(files[0].name);
    },extra_params,0,'image',app);
}

function up_img_for_search(dialog_title,input_selector,extra_params,app){
    open_upload_dialog(dialog_title,function(dialog,files){
        $(input_selector).val(files[0].filepath);
        
        $(input_selector+'_submit').click();
        
    },extra_params,0,'image',app);
}

function refresh_notify_num(){}

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}