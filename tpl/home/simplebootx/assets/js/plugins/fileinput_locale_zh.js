/*!
 * FileInput Spanish (Latin American) Translations
 *
 * This file must be loaded after 'fileinput.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * @see http://github.com/kartik-v/bootstrap-fileinput
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
(function ($) {
    "use strict";
    $.fn.fileinput.locales.es = {
    		fileSingle: '单个图片',
            filePlural: '多个图片',
            browseLabel: '选择图片 &hellip;',
            removeLabel: '删除图片',
            removeTitle: '删除选中图片',
            cancelLabel: '取消',
            cancelTitle: '取消上传',
            uploadLabel: '上传',
            uploadTitle: '上传选中图片',
            msgSizeTooLarge: '图片 "{name}" (<b>{size} KB</b>) 超过允许的最大上传大小 <b>{maxSize} KB</b>. 请重新上传!',
            msgFilesTooLess: '图片数量必须大于 <b>{n}</b> {files} ，请重新上传！',
            msgFilesTooMany: '选择的图片数量 <b>({n})</b> 超过允许的最大允许限制 <b>({m})</b>，请重新上传！',
            msgFileNotFound: '图片 "{name}" 未找到!',
            msgFileSecured: 'Security restrictions prevent reading the file "{name}".',
            msgFileNotReadable: 'File "{name}" is not readable.',
            msgFilePreviewAborted: 'File preview aborted for "{name}".',
            msgFilePreviewError: 'An error occurred while reading the file "{name}".',
            msgInvalidFileType: 'Invalid type for file "{name}". Only "{types}" files are supported.',
            msgInvalidFileExtension: 'Invalid extension for file "{name}". Only "{extensions}" files are supported.',
            msgValidationError: 'File Upload Error',
            msgLoading: 'Loading file {index} of {files} &hellip;',
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            msgSelected: '选中{n}个图片',
            msgFoldersNotAllowed: 'Drag & drop files only! {n} folder(s) dropped were skipped.',
            dropZoneTitle: '拖拽图片到这里...'
    };

    $.extend($.fn.fileinput.defaults, $.fn.fileinput.locales.es);
})(window.jQuery);
