vm = new Vue({
    el: ".page"
    data:
        fixedSelectlistFlag: 0
        viewType: [
            {
                id: 1
                pid: 1
                name: "纯色"
                isShow: 1
            }
            {
                id: 2
                pid: 1
                name: "纯色中纹"
            }
            {
                id: 3
                pid: 1
                name: "纯色粗纹"
            }
            {
                id: 4
                pid: 2
                name: "麻色"
                isShow: 1
            }
            {
                id: 5
                pid: 2
                name: "麻色中纹"
            }
            {
                id: 6
                pid: 2
                name: "麻色粗纹"
            }
            {
                id: 7
                pid: 3
                name: "条纹"
                isShow: 1
            }
            {
                id: 8
                pid: 3
                name: "条纹中纹"
            }
            {
                id: 9
                pid: 3
                name: "条纹粗纹"
            }
            {
                id: 10
                pid: 4
                name: "网格"
                isShow: 1
            }
            {
                id: 11
                pid: 4
                name: "网格中纹"
            }
            {
                id: 12
                pid: 4
                name: "网格粗纹"
            }
            {
                id: 13
                pid: 5
                name: "图案"
                isShow: 1
            }
            {
                id: 14
                pid: 5
                name: "中图案"
            }
            {
                id: 15
                pid: 5
                name: "大图案"
            }
        ]
        imageList: {
            ptxs: [
                {
                    id: [1, 1]
                    src: "/public/v2/assets/img/list/thumb/ptxs/1.jpg"
                    des: '纹路变化周期≤1mm<br />普通纱线，纱线粗细≤1mm'
                }
                {
                    id: [1, 2]
                    src: "/public/v2/assets/img/list/thumb/ptxs/2.jpg"
                    des: "1mm<纹路变化周期≤5mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 3]
                    src: "/public/v2/assets/img/list/thumb/ptxs/3.jpg"
                    des: "纹路变化周期>5mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 4]
                    src: "/public/v2/assets/img/list/thumb/ptxs/4.jpg"
                    des: "纹路变化周期≤1mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 5]
                    src: "/public/v2/assets/img/list/thumb/ptxs/5.jpg"
                    des: "1mm<纹路变化周期≤5mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 6]
                    src: "/public/v2/assets/img/list/thumb/ptxs/6.jpg"
                    des: "纹路变化周期>5mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 7]
                    src: "/public/v2/assets/img/list/thumb/ptxs/7.jpg"
                    des: "最细条纹宽度≤5mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 8]
                    src: "/public/v2/assets/img/list/thumb/ptxs/8.jpg"
                    des: "5mm<最细条纹宽度≤10mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 9]
                    src: "/public/v2/assets/img/list/thumb/ptxs/9.jpg"
                    des: "最细条纹宽度>10mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 10]
                    src: "/public/v2/assets/img/list/thumb/ptxs/10.jpg"
                    des: "网格最小宽度≤5mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 11]
                    src: "/public/v2/assets/img/list/thumb/ptxs/11.jpg"
                    des: "5mm<网格最小宽度≤10mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 12]
                    src: "/public/v2/assets/img/list/thumb/ptxs/12.jpg"
                    des: "网格最小宽度>10mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 13]
                    src: "/public/v2/assets/img/list/thumb/ptxs/13.jpg"
                    des: "图形最小宽度≤5mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 14]
                    src: "/public/v2/assets/img/list/thumb/ptxs/14.jpg"
                    des: "5mm<图形最小宽度≤10mm<br />普通纱线，纱线粗细≤1mm"

                }
                {
                    id: [1, 15]
                    src: "/public/v2/assets/img/list/thumb/ptxs/15.jpg"
                    des: "图形最小宽度>10mm<br />普通纱线，纱线粗细≤1mm"
                }

            ]
            ptcs: [
                {
                    id: [2, 1]
                    src: "/public/v2/assets/img/list/thumb/ptcs/1.jpg"
                    des: "纹路变化周期≤1mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 2]
                    src: "/public/v2/assets/img/list/thumb/ptcs/2.jpg"
                    des: "1mm<纹路变化周期≤5mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 3]
                    src: "/public/v2/assets/img/list/thumb/ptcs/3.jpg"
                    des: "纹路变化周期>5mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 4]
                    src: "/public/v2/assets/img/list/thumb/ptcs/4.jpg"
                    des: "纹路变化周期≤1mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 5]
                    src: "/public/v2/assets/img/list/thumb/ptcs/5.jpg"
                    des: "1mm<纹路变化周期≤5mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 6]
                    src: "/public/v2/assets/img/list/thumb/ptcs/6.jpg"
                    des: "纹路变化周期>5mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 7]
                    src: "/public/v2/assets/img/list/thumb/ptcs/7.jpg"
                    des: "最细条纹宽度≤5mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 8]
                    src: "/public/v2/assets/img/list/thumb/ptcs/8.jpg"
                    des: "5mm<最细条纹宽度≤10mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 9]
                    src: "/public/v2/assets/img/list/thumb/ptcs/9.jpg"
                    des: "最细条纹宽度>10mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 10]
                    src: "/public/v2/assets/img/list/thumb/ptcs/10.jpg"
                    des: "网格最小宽度≤5mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 11]
                    src: "/public/v2/assets/img/list/thumb/ptcs/11.jpg"
                    des: "5mm<网格最小宽度≤10mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 12]
                    src: "/public/v2/assets/img/list/thumb/ptcs/12.jpg"
                    des: "网格最小宽度>10mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 13]
                    src: "/public/v2/assets/img/list/thumb/ptcs/13.jpg"
                    des: "图形最小宽度≤5m<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 14]
                    src: "/public/v2/assets/img/list/thumb/ptcs/14.jpg"
                    des: "5mm<图形最小宽度≤10mm<br />普通纱线，纱线粗细>1mm"

                }
                {
                    id: [2, 15]
                    src: "/public/v2/assets/img/list/thumb/ptcs/15.jpg"
                    des: "图形最小宽度>10mm<br />普通纱线，纱线粗细>1mm"

                }

            ]
            tss: [
                {
                    id: [3, 1]
                    src: "/public/v2/assets/img/list/thumb/tss/1.jpg"
                    des: "纹路变化周期≤1mm<br />特殊纱线"

                }
                {
                    id: [3, 2]
                    src: "/public/v2/assets/img/list/thumb/tss/2.jpg"
                    des: "1mm<纹路变化周期≤5mm<br />特殊纱线"

                }
                {
                    id: [3, 3]
                    src: "/public/v2/assets/img/list/thumb/tss/3.jpg"
                    des: "纹路变化周期>5mm<br />特殊纱线"

                }
                {
                    id: [3, 4]
                    src: "/public/v2/assets/img/list/thumb/tss/4.jpg"
                    des: "纹路变化周期≤1mm<br />特殊纱线"

                }
                {
                    id: [3, 5]
                    src: "/public/v2/assets/img/list/thumb/tss/5.jpg"
                    des: "1mm<纹路变化周期≤5mm<br />特殊纱线"

                }
                {
                    id: [3, 6]
                    src: "/public/v2/assets/img/list/thumb/tss/6.jpg"
                    des: "纹路变化周期>5mm<br />特殊纱线"

                }
                {
                    id: [3, 7]
                    src: "/public/v2/assets/img/list/thumb/tss/7.jpg"
                    des: "最细条纹宽度≤5mm<br />特殊纱线"

                }
                {
                    id: [3, 8]
                    src: "/public/v2/assets/img/list/thumb/tss/8.jpg"
                    des: "5mm<最细条纹宽度≤10mm<br />特殊纱线"

                }
                {
                    id: [3, 9]
                    src: "/public/v2/assets/img/list/thumb/tss/9.jpg"
                    des: "最细条纹宽度>10mm<br />特殊纱线"
                }
#                {
#                    id: 1
#                    src: "/public/v2/assets/img/list/thumb/ptxs/10.jpg"
#                }
#                {
#                    id: 1
#                    src: "/public/v2/assets/img/list/thumb/ptxs/11.jpg"
#                }
#                {
#                    id: 1
#                    src: "/public/v2/assets/img/list/thumb/ptxs/12.jpg"
#                }
#                {
#                    id: 1
#                    src: "/public/v2/assets/img/list/thumb/ptxs/13.jpg"
#                }
#                {
#                    id: 1
#                    src: "/public/v2/assets/img/list/thumb/ptxs/14.jpg"
#                }
#                {
#                    id: 1
#                    src: "/public/v2/assets/img/list/thumb/ptxs/15.jpg"
#                }

            ]
        }
        imageListSimple: {
            small: [
                {
                    id: [1, 1]
                    name: "纯色"
                    src: "/public/v2/assets/img/list/thumb/ptxs/1.jpg"
                    des: '纹路变化周期≤1mm'
                }
                {
                    id: [1, 4]
                    name: "麻色"
                    src: "/public/v2/assets/img/list/thumb/ptxs/4.jpg"
                    des: "纹路变化周期≤1mm"

                }
                {
                    id: [1, 7]
                    name: "条纹"
                    src: "/public/v2/assets/img/list/thumb/ptxs/7.jpg"
                    des: "最细条纹宽度≤5mm"

                }
                  {
                    id: [1, 10]
                    name: "网格"
                    src: "/public/v2/assets/img/list/thumb/ptxs/10.jpg"
                    des: "网格最小宽度≤5mm"

                }
                {
                    id: [1, 13]
                    name: "图案"
                    src: "/public/v2/assets/img/list/thumb/ptxs/13.jpg"
                    des: "图形最小宽度≤5mm"

                }
            ],

            middle: [
                {
                    id: [1, 2]
                    name: "纯色"
                    src: "/public/v2/assets/img/list/thumb/ptxs/2.jpg"
                    des: "1mm<纹路变化周期≤5mm"

                }
                {
                    id: [1, 5]
                    name: "麻色"
                    src: "/public/v2/assets/img/list/thumb/ptxs/5.jpg"
                    des: "1mm<纹路变化周期≤5mm"

                }
                 {
                    id: [1, 8]
                    name: "条纹"
                    src: "/public/v2/assets/img/list/thumb/ptxs/8.jpg"
                    des: "5mm<最细条纹宽度≤10mm"

                }

                {
                    id: [1, 11]
                    name: "网格"
                    src: "/public/v2/assets/img/list/thumb/ptxs/11.jpg"
                    des: "5mm<网格最小宽度≤10mm"

                }
                {
                    id: [1, 14]
                    name: "图案"
                    src: "/public/v2/assets/img/list/thumb/ptxs/14.jpg"
                    des: "5mm<图形最小宽度≤10mm"

                }
                ],
            large: [
                {
                    id: [1, 3]
                    name: "纯色"
                    src: "/public/v2/assets/img/list/thumb/ptxs/3.jpg"
                    des: "纹路变化周期>5mm"

                }
                {
                    id: [1, 6]
                    name: "麻色"
                    src: "/public/v2/assets/img/list/thumb/ptxs/6.jpg"
                    des: "纹路变化周期>5mm"

                }
                 {
                    id: [1, 9]
                    name: "条纹"
                    src: "/public/v2/assets/img/list/thumb/ptxs/9.jpg"
                    des: "最细条纹宽度>10mm"

                }

                {
                    id: [1, 12]
                    name: "网格"
                    src: "/public/v2/assets/img/list/thumb/ptxs/12.jpg"
                    des: "网格最小宽度>10mm"

                }
                {
                    id: [1, 15]
                    name: "图案"
                    src: "/public/v2/assets/img/list/thumb/ptxs/15.jpg"
                    des: "图形最小宽度>10mm"
                }
            ]
        }
        curView: 1
        curCraft: []
        curMaterial: []
        curSubMaterial: []
        curWeave: [1]
        showMoreFlag: 0
        curPageType: 1
        showColor: 0
        showColorTxt: "颜色"
        curSearchData: {
            p: 1
        }
        endFlag: 0
        selectItemList: []
        curColor: "#000000"
        showImageFlag: 0
        showImageSrc: ""
        showImageDes: ""
        curYarnData: []
        tmpYarnData: []
        orderByTime: ""
        orderByPrice: ""
        loadingFlag: 0
        loadTxt: "加载中"
        stopLoadFlag: 0
        selectContainer: {
            isSelectFlag: 0
            containerLeft: 100
        }

    methods:
        showImage: (item) ->

            arrIndex = this.isContain(this.curYarnData, item['id'][0], item['id'][1])
            if this.tmpYarnData[1] % 3 == 0
                yarn = "粗纹"
            else if this.tmpYarnData[1] % 3 == 1
                yarn = "细纹"
            else
                yarn = "中纹"

            if arrIndex >= 0
                #更新了yarndata
                this.changeSelectItem("yarn_view", item['name'] + yarn, item['id'][0] + "_" + item['id'][1])
                
                this.curSearchData['yarn_and_visual'] = this.curYarnData
                this.curSearchData['p'] = 1
                this.loadData()
                return

            this.showImageFlag = 1
            this.showImageSrc = item.src
            this.showImageDes = item.des
            this.changeView(parseInt(item['id'][1] / 3) + 1)
            this.tmpYarnData[0] = item['id'][0]
            this.tmpYarnData[1] = item['id'][1]
            this.tmpYarnData[2] = item['name']

        submitImageSearch: ->
            arrIndex = this.isContain(this.curYarnData, this.tmpYarnData[0], this.tmpYarnData[1])

            this.curYarnData.push [this.tmpYarnData[0], this.tmpYarnData[1]]

            if this.tmpYarnData[1] % 3 == 0
                yarn = "粗纹"
            else if this.tmpYarnData[1] % 3 == 1
                yarn = "细纹"
            else
                yarn = "中纹"

            this.changeSelectItem("yarn_view", this.tmpYarnData[2] + yarn, this.tmpYarnData[0] + "_" + this.tmpYarnData[1])

            this.curSearchData['yarn_and_visual'] = this.curYarnData
            this.showImageFlag = 0
            this.curSearchData['p'] = 1
            this.loadData()
        initColor: ->
            ColorPicker(document.getElementById('color-picker-wrap'), (hex, hsv, rgb) ->
                vm.curColor = hex
            )
        switchColor: ->
            if this.showColor
                this.showColor = 0
                this.showColorTxt = "颜色"
            else
                this.showColor = 1
                this.showColorTxt = "收起"

        scrollView: ->
            scroll = document.getElementById("js-matrix-row-line").scrollLeft
            this.curView = parseInt(scroll / 200) + 1
        submitColorSearch: ->
            this.showColor = 0
            this.showColorTxt = "颜色"
            $('#tf-list ul').html("")
            this.curSearchData = {color: vm.curColor, p: 1}
            this.loadData()
        loadData: (data)->
            if this.stopLoadFlag == 1
                return

            this.loadingFlag = 1
            this.loadTxt = "加载中..."

            this.$http.post(
                "/Tf/List/index?p=" + this.curSearchData['p']
                this.curSearchData
                {
                    emulateJSON: true
                }
            ).then((res) ->
                data = res.body
                $orders = $(data.html);
                this.loadingFlag = 0

                if this.curSearchData['p'] == 1
                    $('#tf-list ul').html($orders);
                else
                    $('#tf-list ul').append($orders);

                if data['data'].length != 0
                    this.endFlag = 0
                else
                    this.endFlag = 1
                    return

                this.curSearchData['p'] = this.curSearchData['p'] + 1
                

                sessionStorage.setItem('ml_list_item', JSON.stringify({
                    html: document.querySelector(".ml-list").innerHTML,
                    search_data: vm.curSearchData,
                    select: vm.selectItemList,
                    material: vm.curMaterial
                    sub_material: vm.curSubMaterial
                    view: vm.curView
                    yarn: vm.curYarnData
                    craft: vm.curCraft
                    weave: vm.curWeave
                }))

            )
        changeSelectItem: (type, name, id) ->
            index = 0
            flag = ""
            document.querySelector("html").scrollTop = 0

            if id == ''
                for item in this.selectItemList
                    if item.type == type
                        this.selectItemList.splice index, 1

                    index++
                return


            for item in this.selectItemList
                
                if item.type == type and item.name == name and item.id == id
                    this.selectItemList.splice index, 1

                    if type is "craft"
                        this.changeCraft(id)
                    else if type is "material"
                        this.changeMaterial(id)
                    else if type is "sub_material"
                        this.changeSubMaterial(id)
                    else if type is "weave"
                        this.changeWeave(id)
                    else if type is "yarn_view"
                        arrIndex = this.isContain(this.curYarnData, id.split("_")[0], id.split("_")[1])

                        console.log id.split("_")[0], id.split("_")[1], arrIndex, this.curYarnData
                        if arrIndex >= 0
                            this.curYarnData.splice arrIndex, 1

                    flag = "delete"
                    break;

                index++

            if flag != "delete"

                this.selectItemList.push {
                    name: name
                    id: id
                    type: type
                }

                if type is "craft"
                    this.changeCraft(id)
                else if type is "material"
                    this.changeMaterial(id)
                else if type is "sub_material"
                    this.changeSubMaterial(id)
                else if type is "weave"
                    this.changeWeave(id)

        changeView: (view) ->
            if view == ''
                this.curSearchData['yarntype'] = ''
                this.curSearchData['visualtype'] = ''
                this.curSearchData['p'] = 1
                this.loadData()

            this.curView  = view
            document.getElementById("js-matrix-row-line").scrollLeft = 220 * (view - 1)
        changeWeave: (item) ->
            if this.curWeave.indexOf(item) >= 0
                this.curWeave.splice(this.curWeave.indexOf(item), 1)
            else
                this.curWeave.push item

        changeCraft: (item) ->
            if this.curCraft.indexOf(item) >= 0
                this.curCraft.splice(this.curCraft.indexOf(item), 1)
            else
                this.curCraft.push item

        changeMaterial: (item) ->

            if this.curMaterial.indexOf(item) >= 0
                this.curMaterial.splice(this.curMaterial.indexOf(item), 1)
            else
                this.curMaterial.push item
        changeSubMaterial: (item) ->

            if this.curSubMaterial.indexOf(item) >= 0
                this.curSubMaterial.splice(this.curSubMaterial.indexOf(item), 1)
            else
                this.curSubMaterial.push item

        isContain: (arr, yarn, visual) ->
            index = 0
            for item in arr
                if item[0] == parseInt(yarn) and item[1] == parseInt(visual)
                    return index
                index++

            return -1
        disabledColorFixedScroll: (e) ->
            e.preventDefault();
        switchSelectContainer: (flag, dom) ->
            if flag
                this.selectContainer.isSelectFlag = 1
                this.selectContainer.containerLeft = 100


                time = setInterval ->
                    if vm.selectContainer.containerLeft > 10
                        vm.selectContainer.containerLeft -= 5
                    else
                        clearTimeout time
                , 10

            else
                if dom.srcElement.className == 'select-fixed-mask'
                    time = setInterval ->
                        if vm.selectContainer.containerLeft < 100
                            vm.selectContainer.containerLeft += 5
                        else
                            clearTimeout time
                            vm.selectContainer.isSelectFlag = 0
                    , 10


    watch:
        orderByTime: ->
            this.curSearchData['orderby'] = this.orderByTime
            this.curSearchData['p'] = 1
            $('#tf-list ul').html("")
            this.loadData()
        orderByPrice: ->
            this.curSearchData['orderby'] = this.orderByPrice
            this.curSearchData['p'] = 1
            $('#tf-list ul').html("")
            this.loadData()

        curWeave: ->
            if this.stopLoadFlag
                return

            this.curSearchData['color'] = ""
            this.curSearchData['p'] = 1
            this.curSearchData['weavetype'] = this.curWeave
            this.loadData()

        curMaterial: ->
            if this.stopLoadFlag
                return

            this.curSearchData['color'] = ""
            this.curSearchData['materialtype'] = this.curMaterial
            this.curSearchData['p'] = 1
            this.loadData()
        curSubMaterial: ->
            if this.stopLoadFlag
                return

            this.curSearchData['color'] = ""
            this.curSearchData['submaterialtype'] = this.curSubMaterial
            this.curSearchData['p'] = 1
            this.loadData()
        curCraft: ->
            if this.stopLoadFlag
                return

            this.curSearchData['color'] = ""
            this.curSearchData['p'] = 1
            this.curSearchData['crafttype'] = this.curCraft
            this.curSearchData['p'] = 1
            this.loadData()

    mounted: ->
        this.initColor()

        window.onscroll = (e) ->
            if $(window).scrollTop() + window.innerHeight + 20 > $("#center").innerHeight() && vm.loadingFlag == 0 && vm.endFlag == 0
                vm.loadData()

            sessionStorage.setItem("ml_list_scroll", $(window).scrollTop() - 10)

            if $(window).scrollTop() > window.innerHeight
                vm.fixedSelectlistFlag = 1
            else
                vm.fixedSelectlistFlag = 0

         ##页面位置初始化
        data = sessionStorage.getItem("ml_list_item")
        if  data and sessionStorage.getItem("detail_enter_flag")
            data = JSON.parse(data)
            this.stopLoadFlag = 1
         
            document.querySelector(".ml-list").innerHTML = data['html']
            this.selectItemList = data['select']
            this.curMaterial = data['material']
            this.curSubMaterial = data['sub_material']
            this.curView = data['view']
            this.curCraft = data['craft']
            this.curWeave = data['weave']
            this.curYarnData = data['yarn']
            this.curSearchData = data['search_data']
            document.querySelector("html").scrollTop = sessionStorage.getItem("ml_list_scroll")
            sessionStorage.removeItem("detail_enter_flag")

            setTimeout ->
                vm.stopLoadFlag = 0
            , 2000
        else
            this.loadData()

})