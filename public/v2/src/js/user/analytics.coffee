myChart = ''

vm = new Vue({
    el: ".app"
    data:
        analyticType: 1
        optionList: [
            {
                value: 1
                name: "每日店铺访问次数"
            }
            {
                value: 2
                name: "每日店铺访客数"
            }
            {
                value: 3
                name: "面料入篮量"
            }
            {
                value: 4
                name: "面料收藏量"
            }
            {
                value: 5
                name: "店铺收藏量"
            }
            {
                value: 6
                name: "意向客户联系数"
            }
        ]
    watch:
        analyticType: ->
            this.getAnalytics()
    methods:
        initCharts: ->
            dom = document.getElementById("charts-container");
            myChart = echarts.init(dom);

        getAnalytics: () ->

            this.$http.post(
                    "/Supplier/Index/get_analytics"
                    {
                        type: this.analyticType
                    }
                    {
                        emulateJSON: true
                    }
            ).then((res) ->
                data = res.body

                xAxisTmp = []
                seriesTmp = []

                for item in this.optionList
                    if item.value is this.analyticType
                        graphName = item.name

                for item in data['data']
                    console.log item
                    xAxisTmp.push item['date']
                    seriesTmp.push item['visit_count']

                console.log xAxisTmp

                option = {
                    title: {
                        text: graphName
                    },
                    xAxis: {
                        data: xAxisTmp
                    },
                    yAxis: {
                        type: 'value'
                        minInterval: 1
                    },
                    series: [{
                        name: '（次）',
                        data: seriesTmp,
                        type: 'line'
                    }]
                }

                myChart.setOption(option)
            )
    mounted: ->
        this.initCharts()

        this.getAnalytics()

})