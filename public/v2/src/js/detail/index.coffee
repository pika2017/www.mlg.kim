window.vm = vm = new Vue({
    el: ".wrapper"
    data:
        curSkuType: 0
        getType: 1
        curSkuId: ''
    methods:
        backHome: ->
            if document.referrer
                javascript:history.back(-1)
            else
                window.location.href = "/"
        changeSwiper: (index, curSkuId) ->
            domArr = $(".view .swiper-slide")

            tmpIndex = 1
#            console.log index
            for item in domArr
                dataIndex = $(item).attr("data-index")
                console.log index, tmpIndex, dataIndex
                if index == parseInt(dataIndex)
                    viewSwiper.slideTo  tmpIndex - 1
                    break;

                tmpIndex++

            this.curSkuId = curSkuId
#            console.log domArr
#            $(".view .swiper-slide")
#            viewSwiper.slideTo  index
        submitBuyReq: ->
            $("#buy-box").submit()
    mounted: ->
        sessionStorage.setItem("detail_enter_flag", 1)
        $(".no-image-sku").css("height", parseInt($(".sku-list img").height()) + 2 + "px")
})