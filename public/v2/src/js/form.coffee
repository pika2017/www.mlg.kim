vm = new Vue({
    el: ".form-wrap"
    data:
        curPage: 3
        pickDate: ""
        fabricData:
            name: ""
            address: ""
            phone: ""
            time: ""
    methods:
        submitForm: ->
            if this.fabricData.name == ""
                alert "请填写档口名称"
                return

            if this.fabricData.address == ""
                alert "请填写档口地址"
                return

            if this.fabricData.phone == ""
                alert "请填写联系电话"
                return

            if this.fabricData.time == ""
                alert "请选择上门取件时间"
                return

            this.$http.post(
                "/Tf/list/submit_form"
                {
                    data: vm.fabricData
                }
                {
                    emulateJSON: true
                }

            ).then((res) ->
                data = res.body

                if data.code is "200"
                    this.curPage = 3
                else
                    alert data.message
            )

    mounted: ->
        this.pickDate = new window.Pikaday
            field: document.getElementById('pick-date')
            format: "D MMMM YYYY"
            yearRange: [2010, 2040]
            onSelect: (date)->
                vm.fabricData.time = moment(date).format('YYYY-MM-DD')
            i18n: {
                previousMonth : '上个月',
                nextMonth     : '下个月',
                months        : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
                weekdays      : ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
                weekdaysShort : ['日','一','二','三','四','五','六']
            }



})