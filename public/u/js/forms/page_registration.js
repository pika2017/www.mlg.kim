var Registration = function () {

    return {

        //Registration Form
        initRegistration: function () {
	        // Validation
	        $("#sky-form4").validate({
	            // Rules for form validation
	            rules:
	            {
	                username:
	                {
	                    required: true
	                },
                    companyName:
	                {
	                    required: true,
	                },
	                password:
	                {
	                    required: true,
	                    minlength: 3,
	                    maxlength: 20
	                },
	                passwordConfirm:
	                {
	                    required: true,
	                    minlength: 3,
	                    maxlength: 20,
	                    equalTo: '#password'
	                },
                    phoneNum:
	                {
	                    required: true,
                        minlength: 11,
                        maxlength: 11
	                },
                    yzm:
	                {
	                    required: true,
                        minlength: 6,
                        maxlength: 6
	                },
                    checkbox:
                    {
                        required:true
                    }
	            },

	            // Messages for form validation
	            messages:
	            {
	                login:
	                {
	                    required: 'Please enter your login'
	                },
                    companyName:
	                {
	                    required: '请输入公司名称，或填写个人'
	                },
	                password:
	                {
	                    required: '请输入密码'
	                },
	                passwordConfirm:
	                {
	                    required: '请再次输入密码',
	                    equalTo: '两次密码不一致'
	                },
                    username:
	                {
	                    required: '请输入用户名'
	                },
                    phoneNum:
	                {
	                    required: '亲输入联系方式',
                        minlength: "请输入11位电话号码"
	                },
	                yzm:
	                {
	                    required: '请输入验证码',
                        minlength: "请输入正确的验证码"
	                },
                    checkbox:
                    {
                        required:'请勾选同意面料馆协议'
                    }
	            },

	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
}();